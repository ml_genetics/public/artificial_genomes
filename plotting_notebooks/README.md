# Inspecting Genetic Datasets created using Generative Models

## Figures

### Summary statistics
**[launch_plotting_figs.ipynb](https://gitlab.inria.fr/ml_genetics/public/artificial_genomes/blob/master/plotting_notebooks/launch_plotting_figs.ipynb)**
automatically launches the computation of different statistics on the AGs (Allele Frequency, PCA, lD, Pairwise distance distributions, ...) and compare them to the real ones.  
All figures are stored in FIG/

The notebook calls successively the following notebooks so that some analyses can be skipped if needed:

```
%run -p plotting_figures_utils_1_INIT.ipynb  # MANDATORY
%run -p plotting_figures_utils_2_AF.ipynb  
%run -p plotting_figures_utils_3_PCA.ipynb  
%run -p plotting_figures_utils_4_LD.ipynb  
%run -p plotting_figures_utils_5_DIST_AATS.ipynb  
```


### RBM Projections
**[projRBM.ipynb](https://gitlab.inria.fr/ml_genetics/private/genegan/blob/master/projRBM.ipynb)** computes the projections of the real data into the visible or hidden space of an RBM model.


