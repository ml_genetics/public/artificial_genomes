import sys
import os
import time
import random
import numpy as np
import pandas as pd
from datetime import datetime
import matplotlib.pyplot as plt
from pca_plot_genomes import pca_plot
from numpy.random import randint
import torch.backends.cudnn as cudnn
from models_65K import * #models_10K for 16383 zero padded SNP data

## Set seed for reproducibility
manualSeed = 9
random.seed(manualSeed)
torch.manual_seed(manualSeed)
print("Random Seed: ", manualSeed)

inpt = "1000G_real_genomes/65K_SNP_1000G_real.hapt" #hapt format input file
out_dir = "./output_dir"
alph = 0.01 #alpha value for LeakyReLU
g_learn = 0.0005 #generator learning rate
d_learn = 0.0005 #discriminator learning rate
epochs = 10001
batch_size = 96
channels = 10 #channel multiplier which dictates the number of channels for all layers
ag_size = 500 #number of artificial genomes (haplotypes) to be created
gpu = 1 #number of GPUs
save_that = 50 #epoch interval for saving outputs
pack_m = 3 #packing amount for the critic
critic_iter = 10 #number of times critic is trained for every generator training
label_noise = 1 #noise for real labels (1: noise, 0: no noise)
noise_dim = 2 #dimension of noise for each noise vector
latent_depth_factor = 14 #14 for 65535 SNP data and 12 for 16383 zero padded SNP data

device = torch.device("cuda:0" if (torch.cuda.is_available() and gpu > 0) else "cpu") #set device to cpu or gpu

## Prepare the training data
#df = pd.read_hdf(inpt, key="df1", mode='r')
df = pd.read_csv(inpt, sep = ' ', header=None)
df = df.sample(frac=1).reset_index(drop=True)
df_noname = df.drop(df.columns[0:2], axis=1)
df_noname = df_noname.values
#df_noname = df_noname - np.random.uniform(0,0.1, size=(df_noname.shape[0], df_noname.shape[1]))
df = df.iloc[0:ag_size,:]
df_noname = torch.Tensor(df_noname)
df_noname = df_noname.to(device)
dataloader = torch.utils.data.DataLoader(df_noname, batch_size=batch_size, shuffle=True, pin_memory=False)

latent_size = int((df_noname.shape[1]+1)/(2**latent_depth_factor)) #set the latent_size

## Create the generator
netG = ConvGenerator(latent_size=latent_size, data_shape=df_noname.shape[1], gpu=gpu, device=device, channels=channels, noise_dim=noise_dim, alph=alph)
netG = netG.float()
if (device.type == 'cuda') and (gpu > 1):
    netG = nn.DataParallel(netG, list(range(gpu)))
netG.to(device)

## Create the critic
netC = ConvDiscriminator(data_shape=df_noname.shape[1], latent_size=latent_size, gpu=gpu, pack_m = pack_m, device=device, channels=channels, alph=alph).to(device)
netC = netC.float()
if (device.type == 'cuda') and (gpu > 1):
    netC = nn.DataParallel(netC, list(range(gpu)))
netC.to(device)

## Optimizers for generator and critic
c_optimizer = torch.optim.Adam(netC.parameters(), lr=d_learn, betas=(0.5, 0.9))
g_optimizer = torch.optim.Adam(netG.parameters(), lr=g_learn, betas=(0.5, 0.9))

label_fake = torch.tensor(1, dtype=torch.float).to(device)
label_real = label_fake * -1
losses = []

## Noise generator function to be used to provide input noise vectors
def noise_generator(size, noise_count, noise_dim, device):
    noise_list = []
    for i in range(2,noise_count*2+1,2):
        noise = torch.normal(mean=0, std=1, size=(size, noise_dim, latent_size*(2**i)-1), device = device)
        noise_list.append(noise)
    return noise_list

## Training Loop
print("Starting Training Loop...")
start_time = time.time()
for epoch in range(epochs):
    #c_loss_real = 0
    #c_loss_fake = 0
    print(f"epoch number {epoch}")

    b = 0

    while b < len(dataloader):
        for param in netC.parameters():
            param.requires_grad = True

        #Update Critic
        for n_critic in range(critic_iter):

            netC.zero_grad(set_to_none=True)

            X_batch_real = next(iter(dataloader))
            X_batch_real = torch.reshape(X_batch_real, (X_batch_real.shape[0], 1, X_batch_real.shape[1]))

            if pack_m > 1:
                for i in range(pack_m-1):
                    temp_batch = next(iter(dataloader))
                    temp_batch = torch.reshape(temp_batch, (temp_batch.shape[0], 1, temp_batch.shape[1]))
                    X_batch_real = torch.cat((X_batch_real, temp_batch), 1)
            b += 1

            #Train Critic with real samples
            c_loss_real = netC(X_batch_real.float())
            c_loss_real = c_loss_real.mean()

            if label_noise != 0:
                label_noise = torch.tensor(random.uniform(0, 0.1), dtype=torch.float, device=device)
                c_loss_real.backward(label_real + label_noise)
            else:
                c_loss_real.backward(label_real)

            for i in range(pack_m):
                latent_samples = torch.normal(mean=0, std=1, size=(batch_size, noise_dim, latent_size), device=device) #create the initial noise to be fed to generator
                noise_list = noise_generator(batch_size, 6, noise_dim, device)
                temp = netG(latent_samples, noise_list)
                if i == 0:
                    X_batch_fake = temp
                else:
                    X_batch_fake = torch.cat((X_batch_fake, temp), 1)

            c_loss_fake = netC(X_batch_fake.detach())
            c_loss_fake = c_loss_fake.mean()
            c_loss_fake.backward(label_fake)

            #Train with gradient penalty
            gp = gradient_penalty(netC, X_batch_real.float(), X_batch_fake.float(), device)
            gp.backward()
            c_loss = c_loss_fake - c_loss_real + gp
            c_optimizer.step()


        for param in netC.parameters():
            param.requires_grad = False

        #Update G network
        netG.zero_grad(set_to_none=True)
        for i in range(pack_m):
            latent_samples = torch.normal(mean=0, std=1, size=(batch_size, noise_dim, latent_size), device=device) #create the initial noise to be fed to generator
            noise_list = noise_generator(batch_size, 6, noise_dim, device)
            temp = netG(latent_samples, noise_list)
            if i == 0:
                X_batch_fake = temp
            else:
                X_batch_fake = torch.cat((X_batch_fake, temp), 1)

        g_loss = netC(X_batch_fake)
        g_loss = g_loss.mean()
        g_loss.backward(label_real)
        g_optimizer.step()

        # Save Losses for plotting later
        #losses.append((c_loss.item(), g_loss.item()))
        losses.append((round(c_loss.item(), 3), (round(g_loss.item(), 3))))

    ## Outputs for assessment at every "save_that" epoch
    if epoch%save_that == 0 or epoch == epochs:
        torch.cuda.empty_cache()
        torch.save({
        'Generator': netG.state_dict(),
        'Critic': netC.state_dict(),
        'G_optimizer': g_optimizer.state_dict(),
        'C_optimizer': c_optimizer.state_dict()},
        f'{out_dir}/{epoch}')

        netG.eval()
        latent_samples = torch.normal(mean=0, std=1, size=(ag_size, noise_dim, latent_size), device=device) #create the initial noise to be fed to generator
        noise_list = noise_generator(ag_size, 6, noise_dim, device)
        with torch.no_grad():
            generated_genomes = netG(latent_samples, noise_list)
        generated_genomes = generated_genomes.cpu().detach().numpy()
        generated_genomes[generated_genomes < 0] = 0
        generated_genomes = np.rint(generated_genomes)
        generated_genomes_df = pd.DataFrame(np.reshape(generated_genomes, (ag_size, generated_genomes.shape[2])))
        generated_genomes_df = generated_genomes_df.astype(int)
        gen_names = list()

        for i in range(0,len(generated_genomes_df)):
            gen_names.append('AG'+str(i))
        generated_genomes_df.insert(loc=0, column='Type', value="AG")
        generated_genomes_df.insert(loc=1, column='ID', value=gen_names)
        generated_genomes_df.columns = list(range(generated_genomes_df.shape[1]))
        df.columns = list(range(df.shape[1]))

        #Output AGs in hapt or hdf formats
        #generated_genomes_df.to_csv(f'{out_dir}/{epoch}_output.hapt', sep=" ", header=False, index=False)
        generated_genomes_df.to_hdf(f'{out_dir}/{epoch}_output.hapt', key="df1", mode="w")

        #Output lossess
        pd.DataFrame(losses).to_csv(f'{out_dir}/{epoch}_losses.txt', sep=" ", header=False, index=False)
        fig, ax = plt.subplots()
        plt.plot(np.array([losses]).T[0], label='Critic')
        plt.plot(np.array([losses]).T[1], label='Generator')
        plt.title("Training Losses")
        plt.legend()
        fig.savefig(f'{out_dir}/{epoch}_loss.pdf', format='pdf')

        #Plot PCA
        pca_plot(df, generated_genomes_df, epoch, dir=out_dir)

        netG.train()
print("--- %s seconds ---" % (time.time() - start_time))
