import sys
import os
import time
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('pdf')
import matplotlib.pyplot as plt
import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
from models_10K_VAE import *
from pca_plot_genomes import pca_plot
from sklearn.decomposition import PCA
from torch.nn import functional as F

inpt = "1000G_real_genomes/10K_SNP_1000G_real_PADDED.hapt" #hdf format input file
out_dir = "./output_dir"
epochs = 10001
lr = 0.001
batch_size = 256
channels = 8
ag_size = 500 #number of artificial genomes (haplotypes) to be created
gpu = 1 #number of GPUs
alph = 0.001 #alpha value for LeakyReLU
save_that = 50 #epoch interval for saving outputs
noise_dim = 1 #dimension of latent_space

## Prepare the training data
#df = pd.read_hdf(inpt, key="df1", mode='r')
df = pd.read_csv(inpt, sep = ' ', header=None)
df = df.sample(frac=1).reset_index(drop=True)
df_noname = df.drop(df.columns[0:2], axis=1)
df_noname = df_noname.values
df = df.iloc[0:ag_size,:]
dataloader = torch.utils.data.DataLoader(df_noname, batch_size=batch_size, shuffle=True, pin_memory=True)

latent_size = int((df_noname.shape[1]+1)/(2**12))

device = torch.device("cuda:0" if (torch.cuda.is_available() and gpu > 0) else "cpu")

vae = VAE(data_shape=df_noname.shape[1], latent_size=latent_size, channels=channels, noise_dim = noise_dim, alph=alph)
if (device.type == 'cuda') and (gpu > 1):
    vae = nn.DataParallel(vae, list(range(gpu)))
vae.to(device)


optimizer = torch.optim.Adam(vae.parameters(), lr=lr)

losses = []

print("Starting Training Loop...")
start_time = time.time()

#Training
for epoch in range(epochs):
    train_loss = 0.0
    for x in dataloader:
        x = x.to(device)
        x = torch.reshape(x, (x.shape[0], 1, x.shape[1])).float()
        optimizer.zero_grad()
        x_hat = vae(x)
        loss = F.binary_cross_entropy(x_hat, x, reduction="sum") + vae.encoder.kl
        loss.backward()
        optimizer.step()
        train_loss += loss.item()
    #train_loss = train_loss/len(dataloader)
    print('Epoch: {} \tLoss: {:.6f}'.format(epoch, train_loss))
    losses.append(round(train_loss, 3))

    ## Outputs for assessment at every "save_that" epoch
    if epoch%save_that == 0 or epoch == epochs:
        torch.cuda.empty_cache()

        torch.save({
        'VAE': vae.state_dict(),
        'Encoder': vae.encoder.state_dict(),
        'Decoder': vae.decoder.state_dict()},
        f'{out_dir}/{epoch}')

        vae.eval()
        latent_samples = torch.normal(mean=0, std=1, size=(ag_size, 1, latent_size), device = device)
        with torch.no_grad():
            generated_genomes = vae.decoder(latent_samples)
        generated_genomes = generated_genomes.detach().cpu().numpy()
        generated_genomes[generated_genomes < 0] = 0
        generated_genomes = np.rint(generated_genomes)
        generated_genomes = generated_genomes.reshape(generated_genomes.shape[0],generated_genomes.shape[2])
        generated_genomes_df = pd.DataFrame(generated_genomes)
        generated_genomes_df = generated_genomes_df.astype(int)

        gen_names = list()
        for i in range(0,len(generated_genomes_df)):
            gen_names.append('AG'+str(i))
        generated_genomes_df.insert(loc=0, column='Type', value="AG")
        generated_genomes_df.insert(loc=1, column='ID', value=gen_names)
        generated_genomes_df.columns = list(range(generated_genomes_df.shape[1]))
        df.columns = list(range(df.shape[1]))

        generated_genomes_df.to_hdf(f'{out_dir}/{epoch}_output.hapt', key="df1", mode="w")

        pd.DataFrame(losses).to_csv(f'{out_dir}/{epoch}_losses.txt', sep=" ", header=False, index=False)
        fig = plt.figure()
        plt.plot(losses, label='Loss')
        plt.title("Training Loss")
        plt.legend()
        fig.savefig(f'{out_dir}/{epoch}_loss.pdf', format='pdf')

        pca_plot(df, generated_genomes_df, epoch, dir=out_dir)

        vae.train()
print("--- %s seconds ---" % (time.time() - start_time))
