import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
from sklearn.decomposition import PCA
import pandas as pd
from numpy.random import RandomState
from sklearn.model_selection import train_test_split

prng = RandomState(1234567890)

# Decide which device we want to run on
ngpu=1
device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")
if device.type == 'cuda':
    print(torch.cuda.get_device_name(0))
    print('Memory Usage:')
    print('Allocated:', round(torch.cuda.memory_allocated(0)/1024**3,1), 'GB')
    print('Cached:   ', round(torch.cuda.memory_reserved(0)/1024**3,1), 'GB')
    
def reduceDim(proj,percentage):
    #proj : number of PCs/dimensions of PCA of original data
    #percentage : how many percents of these dimensions you want to keep
    #output an integer
    return int(percentage*proj/100)

inpt = "../1000G_real_genomes/1000G_chr1_first_65535.recode.hapt"
df= pd.read_csv(inpt, sep = ' ', header=None)
df=df.values[:,2:]
df=df.astype(int)
np.random.shuffle(df)

pca=PCA()
proj=pca.fit_transform(df)

N_samples=proj.shape[0]
dim_y=proj.shape[1]

alph = 0.01 #alpha value for LeakyReLU
g_learn = 0.0001 #generator learning rate
d_learn = 0.0008 #discriminator learning rate
epochs = 1300
batch_size = 32
ag_size = N_samples #number of artificial genomes (haplotypes) to be created
regularizer=0.0001
beta1 = 0.9

#nComp is different if you look at pca or real data since number of PCs is at most = min(n_features,n_samples)
nComp=reduceDim(min(dim_y,N_samples),90)

dataloader=torch.utils.data.DataLoader(torch.tensor(proj[:,:nComp]),batch_size=batch_size)

latent_size=int(1.2*nComp)

# Generator Code

class Generator(nn.Module):
    def __init__(self):
        super(Generator, self).__init__()
        self.main = nn.Sequential(
            # input is Z, going into a FC
            nn.Linear(latent_size,int(latent_size//1.05)),
            nn.BatchNorm1d(int(latent_size//1.05)),
            nn.LeakyReLU(alph),
            nn.Linear(int(latent_size//1.05),int(latent_size//1.12)),
            nn.BatchNorm1d(int(latent_size//1.12)),
            nn.LeakyReLU(alph),
            nn.Linear(int(latent_size//1.12),int(nComp)),
        )
        self.double()

    def forward(self, input):
        return self.main(input)

# Create the generator
netG = Generator().to(device)
print(netG)

#make critic for WGAN
alph=.01

class Critic(nn.Module):
    def __init__(self):
        super(Critic, self).__init__()
        
        self.main = nn.Sequential(
            nn.Linear(dim_y,int(dim_y//2)),
            nn.LeakyReLU(alph),
            nn.Linear(int(dim_y//2),int(dim_y//3)),
            nn.LeakyReLU(alph),
            nn.Linear(int(dim_y//3),1),
        )
        self.double()
        
    def forward(self, input):
        return self.main(input)

# Create the Discriminator
netC = Critic().to(device)
print(netC)

# Setup Adam optimizers for both G and D
c_optimizer = torch.optim.RMSprop(netC.parameters(), lr=d_learn, weight_decay=0.0001)
g_optimizer = torch.optim.RMSprop(netG.parameters(), lr=g_learn, weight_decay=0.0001)

# Lists to keep track of progress
losses = []
critic_iter = 10
LAMBDA_GP = 0.01

def gradient_penalty(netC, X_real_batch, X_fake_batch, device):
    #batch_size, nb_snps= X_real_batch.shape
    batch_size, nb_snps= X_real_batch.shape[0], X_real_batch.shape[1]
    #alpha is selected randomly between 0 and 1
    alpha= torch.rand(batch_size,1, device=device).repeat(1, nb_snps)
    alpha = alpha.reshape(alpha.shape[0], 1, alpha.shape[1])
    # interpolated image=randomly weighted average between a real and fake image
    #interpolated image ← alpha *real image  + (1 − alpha) fake image
    interpolation=(alpha*X_real_batch) + (1-alpha) * X_fake_batch
    interpolation = interpolation.double()
    interpolation.requires_grad_()
    
    # calculate the critic score on the interpolated image
    interpolated_score= netC(interpolation)
    
    # take the gradient of the score wrt to the interpolated image
    gradient= torch.autograd.grad(inputs=interpolation,
                                  outputs=interpolated_score,
                                  retain_graph=True,
                                  create_graph=True,
                                  grad_outputs=torch.ones_like(interpolated_score),
                                  only_inputs=True
                                 )[0]
    gradient= gradient.view(gradient.shape[0],-1)
    gradient_norm= gradient.norm(2,dim=1)
    gradient_penalty=torch.mean((gradient_norm-1)**2)
    
    return gradient_penalty

real_label=1.
fake_label=0.

for epoch in range(epochs):
    
    for i, data in enumerate(dataloader, 0):
        if i>(N_samples//batch_size)-1:
            break

        ###################
        # (1) Update Critic
        ###################
        for n_critic in range(critic_iter):

            netC.zero_grad(set_to_none=True)          
            # Get the real batch
            X_batch_real = data.to(device)
            # Forward pass real batch through C
            c_pred_real= netC(X_batch_real).view(-1,1)
            # Generate fake image batch with G
            latent_samples = torch.normal(mean=0, std=1, size=(batch_size, latent_size),device=device).double() #create noise to be fed to generator
            X_batch_fake = netG(latent_samples)
            
            gp = gradient_penalty(netC, X_batch_real, X_batch_fake,device)
            c_pred_fake = netC(X_batch_fake.detach()).view(-1,1)
            c_loss = -(torch.mean(c_pred_real) - torch.mean(c_pred_fake)) + LAMBDA_GP *gp
            c_loss.backward()
            c_optimizer.step()     
        
        ######################
        # (2) Update G network
        ######################
        #make discriminator untrainable and train Generator

        netG.zero_grad(set_to_none=True)
        z = torch.normal(mean=0, std=1, size=(batch_size, latent_size),device=device).double() #create noise to be fed to generator
        X_batch_fake = netG(z)
        g_loss = netC(X_batch_fake).view(-1,1)
        g_loss = -g_loss.mean(0).view(1)
        g_loss.backward()
        g_optimizer.step()      

        print('[%d/%d][%d/%d]\tLoss_D: %.4f\tLoss_G: %.4f\t'% (epoch, epochs, i, len(dataloader),c_loss, g_loss)+'\n')
        
print("END training")
