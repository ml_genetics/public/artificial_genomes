To compute diversity, powerlaw fit of diversity and entropy of k-SNP motifs,
add `%run -p {dirscript}/plotfig_utils_7_kSNPmotifs.ipynb` to the cell starting with `%run -p {dirscript}/plotfig_utils_1_INIT.ipynb`

This latter cell can be find in launch_plotting_figs_805_SNP_1000G.ipynb
 https://gitlab.inria.fr/ml_genetics/public/artificial_genomes/-/tree/master/plotting_notebooks?ref_type=heads

The source script of k-SNP motifs related quantities: https://gitlab.inria.fr/ml_genetics/public/artificial_genomes/-/blob/master/plotting_notebooks/short/plotfig_utils_7_kSNPmotifs.ipynb