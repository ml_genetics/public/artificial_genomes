import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
from sklearn.decomposition import PCA
import pandas as pd
from numpy.random import RandomState
from sklearn.model_selection import train_test_split

prng = RandomState(1234567890)

# Decide which device we want to run on
ngpu=1
device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")
if device.type == 'cuda':
    print(torch.cuda.get_device_name(0))
    print('Memory Usage:')
    print('Allocated:', round(torch.cuda.memory_allocated(0)/1024**3,1), 'GB')
    print('Cached:   ', round(torch.cuda.memory_reserved(0)/1024**3,1), 'GB')

inpt = "../1000G_real_genomes/1000G_chr1_first_65535.recode.hapt"
df= pd.read_csv(inpt, sep = ' ', header=None)
df=df.values[:,2:]
df=df.astype(int)
np.random.shuffle(df)

dim_y=df.shape[1]

num_pieces=3

df_piece1=df[:,:dim_y//num_pieces]
df_piece2=df[:,dim_y//num_pieces:2*dim_y//num_pieces]
df_piece3=df[:,2*dim_y//num_pieces:]

del df

#2000 nComps corresponds to ~90% variance explained
pca_piece1,pca_piece2,pca_piece3 = PCA(n_components=2000), PCA(n_components=2000), PCA(n_components=2000)
proj_piece1,proj_piece2,proj_piece3 = pca_piece1.fit_transform(df_piece1), pca_piece2.fit_transform(df_piece2), pca_piece3.fit_transform(df_piece3)

del df_piece1
del df_piece2
del df_piece3

concat=np.concatenate([proj_piece1,proj_piece2,proj_piece3],axis=1)

Ns,Nv=concat.shape


alph = 0.01 #alpha value for LeakyReLU
g_learn = 0.0001 #generator learning rate
d_learn = 0.0008 #discriminator learning rate
epochs = 1101
batch_size = 32
ag_size = Ns #number of artificial genomes (haplotypes) to be created
regularizer=0.0001
beta1 = 0.9

dataloader=torch.utils.data.DataLoader(torch.tensor(concat),batch_size=batch_size)

del concat

latent_size=Nv
dim_y=Nv

# Generator Code

class Generator(nn.Module):
    def __init__(self):
        super(Generator, self).__init__()
        self.main = nn.Sequential(
            # input is Z, going into a FC
            nn.Linear(latent_size,latent_size),
            #nn.BatchNorm1d(int(latent_size//1.05)),
            nn.LeakyReLU(alph),
            nn.Linear(latent_size,latent_size),
            nn.LeakyReLU(alph),
            nn.Linear(latent_size,latent_size),
        )
        self.double()

    def forward(self, input):
        return self.main(input)

# Create the generator
        
netG = Generator().to(device)

#make critic for WGAN
alph=.01

class Critic_global(nn.Module):
    def __init__(self):
        super(Critic_global, self).__init__()
        
        self.main = nn.Sequential(
            nn.Linear(dim_y,int(dim_y//2)),
            nn.LeakyReLU(alph),
            nn.Linear(int(dim_y//2),int(dim_y//3)),
            nn.LeakyReLU(alph),
            nn.Linear(int(dim_y//3),1),
        )
        self.double()
        
    def forward(self, input):
        return self.main(input)

# Create the Discriminator
netC_global = Critic_global().to(device)
print(netC_global)


dim_y_1=2000

class Critic_local_piece1(nn.Module):
    def __init__(self):
        super(Critic_local_piece1, self).__init__()
        
        self.main = nn.Sequential(
            nn.Linear(dim_y_1,int(dim_y_1//2)),
            nn.LeakyReLU(alph),
            nn.Linear(int(dim_y_1//2),int(dim_y_1//3)),
            nn.LeakyReLU(alph),
            nn.Linear(int(dim_y_1//3),1),
        )
        self.double()
        
    def forward(self, input):
        return self.main(input)
    
# Create the Discriminator
netC_local_1 = Critic_local_piece1().to(device)

class Critic_local_piece2(nn.Module):
    def __init__(self):
        super(Critic_local_piece2, self).__init__()
        
        self.main = nn.Sequential(
            nn.Linear(dim_y_1,int(dim_y_1//2)),
            nn.LeakyReLU(alph),
            nn.Linear(int(dim_y_1//2),int(dim_y_1//3)),
            nn.LeakyReLU(alph),
            nn.Linear(int(dim_y_1//3),1),
        )
        self.double()
        
    def forward(self, input):
        return self.main(input)
    
# Create the Discriminator
netC_local_2 = Critic_local_piece2().to(device)

class Critic_local_piece3(nn.Module):
    def __init__(self):
        super(Critic_local_piece3, self).__init__()
        
        self.main = nn.Sequential(
            nn.Linear(dim_y_1,int(dim_y_1//2)),
            nn.LeakyReLU(alph),
            nn.Linear(int(dim_y_1//2),int(dim_y_1//3)),
            nn.LeakyReLU(alph),
            nn.Linear(int(dim_y_1//3),1),
        )
        self.double()
        
    def forward(self, input):
        return self.main(input)
    
# Create the Discriminator
netC_local_3 = Critic_local_piece3().to(device)

# Setup Adam optimizers for both G and D
c_global_optimizer = torch.optim.RMSprop(netC_global.parameters(), lr=d_learn, weight_decay=0.0001)
c_local_1_optimizer = torch.optim.RMSprop(netC_local_1.parameters(), lr=d_learn, weight_decay=0.0001)
c_local_2_optimizer = torch.optim.RMSprop(netC_local_2.parameters(), lr=d_learn, weight_decay=0.0001)
c_local_3_optimizer = torch.optim.RMSprop(netC_local_3.parameters(), lr=d_learn, weight_decay=0.0001)
g_optimizer = torch.optim.RMSprop(netG.parameters(), lr=g_learn, weight_decay=0.0001)

critic_iter = 10
LAMBDA_GP = 0.01

def gradient_penalty(netC, X_real_batch, X_fake_batch, device):
    #batch_size, nb_snps= X_real_batch.shape
    batch_size, nb_snps= X_real_batch.shape[0], X_real_batch.shape[1]
    #alpha is selected randomly between 0 and 1
    alpha= torch.rand(batch_size,1, device=device).repeat(1, nb_snps)
    alpha = alpha.reshape(alpha.shape[0], 1, alpha.shape[1])
    # interpolated image=randomly weighted average between a real and fake image
    #interpolated image ← alpha *real image  + (1 − alpha) fake image
    interpolation=(alpha*X_real_batch) + (1-alpha) * X_fake_batch
    interpolation = interpolation.double()
    interpolation.requires_grad_()
    
    # calculate the critic score on the interpolated image
    interpolated_score= netC(interpolation)
    
    # take the gradient of the score wrt to the interpolated image
    gradient= torch.autograd.grad(inputs=interpolation,
                                  outputs=interpolated_score,
                                  retain_graph=True,
                                  create_graph=True,
                                  grad_outputs=torch.ones_like(interpolated_score),
                                  only_inputs=True
                                 )[0]
    gradient= gradient.view(gradient.shape[0],-1)
    gradient_norm= gradient.norm(2,dim=1)
    gradient_penalty=torch.mean((gradient_norm-1)**2)
    
    return gradient_penalty

real_label=1.
fake_label=0.

for epoch in range(epochs):
    
    for i, data in enumerate(dataloader, 0):
        if i>(Ns//batch_size)-1:
            break

        ###################
        # (1) Update Critic
        ###################
        for n_critic in range(critic_iter):
            
            ###################
            # (1) Update global critic
            ###################
            netC_global.zero_grad(set_to_none=True)          
            # Get the real batch
            ###COMMON TO EVERY CRITICS
            X_batch_real = data.to(device)
            # Forward pass real batch through C
            c_global_pred_real= netC_global(X_batch_real).view(-1,1)
            # Generate fake image batch with G
            latent_samples = torch.normal(mean=0, std=1, size=(batch_size, latent_size),device=device).double() #create noise to be fed to generator
            ###COMMON TO EVERY CRITICS
            X_batch_fake = netG(latent_samples)
            
            gp_global = gradient_penalty(netC_global, X_batch_real, X_batch_fake,device)
            c_global_pred_fake = netC_global(X_batch_fake.detach()).view(-1,1)
            c_global_loss = -(torch.mean(c_global_pred_real) - torch.mean(c_global_pred_fake)) + LAMBDA_GP*gp_global
            
            #split into chunks
            chunks_real = torch.chunk(X_batch_real, num_pieces, dim=1)
            chunks_fake = torch.chunk(X_batch_fake, num_pieces, dim=1)
            
            ###################
            # (1) Update Critic_local_piece1
            ###################
            netC_local_1.zero_grad(set_to_none=True)
            # Forward pass real batch through C
            c_local_1_pred_real= netC_local_1(chunks_real[0]).view(-1,1)
            gp_1 = gradient_penalty(netC_local_1, chunks_real[0], chunks_fake[0],device)
            c_local_1_pred_fake = netC_local_1(chunks_fake[0].detach()).view(-1,1)
            c_local_1_loss = -(torch.mean(c_local_1_pred_real) - torch.mean(c_local_1_pred_fake)) + LAMBDA_GP*gp_1
            
            ###################
            # (1) Update Critic_local_piece2
            ###################
            netC_local_2.zero_grad(set_to_none=True)            
            # Forward pass real batch through C
            c_local_2_pred_real= netC_local_2(chunks_real[1]).view(-1,1)
            gp_2 = gradient_penalty(netC_local_2, chunks_real[1], chunks_fake[1],device)
            c_local_2_pred_fake = netC_local_2(chunks_fake[1].detach()).view(-1,1)
            c_local_2_loss = -(torch.mean(c_local_2_pred_real) - torch.mean(c_local_2_pred_fake)) + LAMBDA_GP*gp_2            
            
            ###################
            # (1) Update Critic_local_piece3
            ###################
            netC_local_3.zero_grad(set_to_none=True)            
            # Forward pass real batch through C
            c_local_3_pred_real= netC_local_3(chunks_real[2]).view(-1,1)
            gp_3 = gradient_penalty(netC_local_3, chunks_real[2], chunks_fake[2],device)
            c_local_3_pred_fake = netC_local_3(chunks_fake[2].detach()).view(-1,1)
            c_local_3_loss = -(torch.mean(c_local_3_pred_real) - torch.mean(c_local_3_pred_fake)) + LAMBDA_GP*gp_3
            
            critics_loss=c_global_loss+c_local_1_loss+c_local_2_loss+c_local_3_loss
            critics_loss.backward()
            c_global_optimizer.step()
            c_local_1_optimizer.step()
            c_local_2_optimizer.step()
            c_local_3_optimizer.step()
        
        ######################
        # (2) Update G network
        ######################
        #make discriminator untrainable and train Generator

        del X_batch_real
        del X_batch_fake
        del chunks_real
        del chunks_fake
        del latent_samples
        
        netG.zero_grad(set_to_none=True)
        z = torch.normal(mean=0, std=1, size=(batch_size, latent_size),device=device).double() #create noise to be fed to generator
        X_batch_fake = netG(z)
        chunks_fake = torch.chunk(X_batch_fake, num_pieces, dim=1)
        g_loss_pred_global = netC_global(X_batch_fake).view(-1,1)
        g_loss_pred_local_1 = netC_local_1(chunks_fake[0]).view(-1,1)
        g_loss_pred_local_2 = netC_local_2(chunks_fake[1]).view(-1,1)
        g_loss_pred_local_3 = netC_local_3(chunks_fake[2]).view(-1,1)
        g_loss = -(g_loss_pred_global.mean(0).view(1)+g_loss_pred_local_1.mean(0).view(1)+g_loss_pred_local_2.mean(0).view(1)+g_loss_pred_local_3.mean(0).view(1))
        g_loss.backward()
        g_optimizer.step()
        
        del z
        del X_batch_fake
        del chunks_fake
        
    print('[%d/%d]\tLoss_C_global: %.4f\tLoss_C1: %.4f\tLoss_C2: %.4f\tLoss_C3: %.4f\tLoss_G: %.4f\tGP: %.4f\tGP1: %.4f\tGP2: %.4f\tGP3: %.4f'% (epoch, epochs,c_global_loss,c_local_1_loss,c_local_2_loss,c_local_3_loss, g_loss, gp_global,gp_1,gp_2,gp_3))

print("END training")
