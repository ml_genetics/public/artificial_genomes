print('Start importing required libraries...')
import os, sys, time
import subprocess
sys.path.append('../')
from tqdm.auto import tqdm
import allel
import yaml
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import torch
torch.autograd.set_detect_anomaly(True)
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import math
from collections import Counter
import gzip
#import tagore
from scipy.interpolate import interp1d

import random

import argparse
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils

import dataclasses
from typing import Sequence
import functools
from typing import Tuple  # Add this line to import Tuple
from torch import optim
import pytorch_warmup as warmup

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn import preprocessing

from sklearn.metrics import confusion_matrix

from lainet.models.network_constructor import get_network
from lainet.utils.eval import compute_accuracy, AccuracyLogger, complete_sk_eval, print_sk_eval
from lainet.utils.reader import load_founders_from_vcf_and_map, load_results_file
from lainet.utils.output_writer import get_meta_data, write_msp_tsv
from lainet.training import train_main, eval_predictions
from lainet.inference import inference_main 

from utils.format_file import add_result, naming_file, output_name
from utils.format_file import naming_file, extract_from_same_file, extract_from_file,output_name_mixed, extract_from_file_antoine

print('Done importing')

# Decide which device we want to run on
ngpu=1
device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")

if device.type == 'cuda':
    print(torch.cuda.get_device_name(0))
    print('Memory Usage:')
    print('Allocated:', round(torch.cuda.memory_allocated(0)/1024**3,1), 'GB')
    print('Cached:   ', round(torch.cuda.memory_reserved(0)/1024**3,1), 'GB')
 
# util function
def flatten(xss):
    return [x for xs in xss for x in xs]

def readVCF(filename,n_snps=500):
    """
    Parse output vcf of ancestry predictions along windows of sizes n_snps into pandas dataframe
    it takes the max ocurring ancestry in a window as the ancestry of that window
    """
    f = open(filename, "r")
    columns=["chm", "spos", "epos", "sgpos", "egpos", "n snps"]
    df = pd.DataFrame(columns=["chm", "spos", "epos", "sgpos", "egpos", "n snps"])
    currSNP=0
    spos=-1
    ancestryCount = []
    stupidL=[]
    epos=-1
    for line in f:

        if line[0] == "#" and line[1] == "#":
            pass
        elif line[0] == "#" and line[1] == "C":
            curr = line.split()
            for elt in curr[9:]:
                columns.append(f'{elt}.0')
                columns.append(f'{elt}.1')
                ancestryCount.append({})
                ancestryCount.append({})
                stupidL.append(f'{elt}.0')
                stupidL.append(f'{elt}.1')
            df = pd.DataFrame(columns=columns)
        else:
            curr = line.split()
            if currSNP==0:
                spos=curr[1]
            if currSNP%n_snps==0 and currSNP>0:
                newrow={"chm":curr[0],"spos":spos,"epos":epos,"sgpos":1, "egpos":1,"n snps":n_snps}
                for j in range(9,len(curr)):
                    for i in range(2):
                        max=0
                        ind=""
                        for key in ancestryCount[(j-9)*2+i]:
                            if ancestryCount[(j-9)*2+i][key]>max:
                                max=ancestryCount[(j-9)*2+i][key]
                                ind=key
                        ancestryCount[(j-9)*2+i]={}
                        newrow[stupidL[(j-9)*2+i]]=key
                spos=curr[1]
                df.loc[len(df)]=newrow
                #df.append(newrow, ignore_index=True)
            for j in range(9,len(curr)):
                anc=curr[j].split(":")[1].split(",")
                for i in range(2):

                    if anc[i] in ancestryCount[(j-9)*2+i]:
                        ancestryCount[(j-9)*2+i][anc[i]]+=1
                    else:
                        ancestryCount[(j-9)*2+i][anc[i]]=1
            currSNP+=1
            epos=curr[1]
            
    df.loc[len(df)-1,"epos"]=curr[1]
    
    return df

# Arguments
genetic_map_file =  "../data/input/real/allchrs_b37.gmap"
config_path = 'configs/default.yaml'

def bcast_right(x: torch.Tensor, ndim: int) -> torch.Tensor:
    """Util function for broadcasting to the right."""
    if x.ndim > ndim:
        raise ValueError(f'Cannot broadcast a value with {x.ndim} dims to {ndim} dims.')
    elif x.ndim < ndim:
        difference = ndim - x.ndim
        return x.view(x.shape + (1,) * difference)
    else:
        return x

    
class DiscreteDDPMProcess:
    """A Gaussian diffusion process: q(xt|x0) = N(alpha(t)*x0, sigma(t)^2 * I),
    which implies the following transition from x0 to xt:

    xt = alpha(t) x0 + sigma(t) eps, eps ~ N(0, I).

    Diffusion processes differ in how they specify alpha(t) and/or sigma(t).
    Here we follow the DDPM paper.

    """
    def __init__(
        self,
        num_diffusion_timesteps: int = 1000,
        beta_start: float = 0.0001,
        beta_end: float = 0.02,
    ):
        self._num_diffusion_timesteps = num_diffusion_timesteps
        self._beta_start = beta_start
        self._beta_end = beta_end
        self._betas = np.linspace(self._beta_start, self._beta_end, self._num_diffusion_timesteps)

        alphas_bar = self._get_alphas_bar()
        self._alphas = torch.tensor(np.sqrt(alphas_bar), dtype=torch.float32)
        self._sigmas = torch.tensor(np.sqrt(1 - alphas_bar), dtype=torch.float32)

    @property
    def tmin(self):
        return 1

    @property
    def tmax(self):
        return self._num_diffusion_timesteps

    def _get_alphas_bar(self) -> np.ndarray:
        alphas_bar = np.cumprod(1.0 - self._betas)

        # we can add this 1 in front to simplify indexing,
        # and to make alpha[0]=1 and sigma[0]=0.
        # these values at t=0 will be needed later when generating samples
        alphas_bar = np.concatenate(([1.], alphas_bar))

        return alphas_bar

    def alpha(self, t: torch.Tensor) -> torch.Tensor:
        return self._alphas.to(device)[t.long()]

    def sigma(self, t: torch.Tensor) -> torch.Tensor:
        return self._sigmas.to(device)[t.long()]

    def sample(self, x0: torch.Tensor, t: torch.Tensor, eps: torch.Tensor) -> torch.Tensor:
        """Draws samples from the forward diffusion process q(xt|x0)."""
        alpha_t = self.alpha(t).view(-1,1)#view(x0.shape[0] )
        sigma_t = self.sigma(t).view(-1,1)#.view(x0.shape[0] )
        return alpha_t * x0 + sigma_t * eps

class SinusoidalTimeEmbedding(nn.Module):
    """Time (positional) embedding as in Transformers."""

    def __init__(self, num_features: int, name: str = None):
        super(SinusoidalTimeEmbedding, self).__init__()
        self._num_features = num_features

    def forward(self, inputs: torch.Tensor) -> torch.Tensor:
        assert len(inputs.shape) == 1
        half_dim = self._num_features // 2
        e = torch.log(torch.tensor(10000.0)) / (half_dim - 1)
        embedding = torch.exp(-e * torch.arange(half_dim).float()).to(device)
        embedding = inputs.view(-1, 1) * embedding
        embedding = torch.cat([torch.cos(embedding), torch.sin(embedding)], dim=-1)
        if self._num_features % 2 == 1:
            embedding = nn.functional.pad(embedding, (0, 1))
        return embedding

class UniformDiscreteTimeSampler:

    def __init__(self, tmin: int, tmax: int):
        self._tmin = tmin
        self._tmax = tmax

    def sample(self, shape: Sequence[int]) -> torch.Tensor:
        return torch.randint(low=self._tmin, high=self._tmax, size=shape)

class ResidualMLP(nn.Module):
    """MLP with residual connections."""

    def __init__(
        self,
        n_blocks: int,
        n_hidden: int,
        n_out: int,
        activation: str,
        name: str = None
    ):
        super(ResidualMLP, self).__init__()
        self._n_blocks = n_blocks
        self._n_hidden = n_hidden
        self._n_out = n_out
        self._activation = getattr(nn.functional, activation)
        
        self.label_emb=nn.Embedding(3,3)

        self.linear_input = nn.Linear(n_out+3, n_hidden)
        self.linear_time_1 = nn.Linear(n_hidden, n_hidden)
        self.linear_hidden_1_1 = nn.Linear(n_hidden, n_hidden)
        self.linear_hidden_1_2 = nn.Linear(n_hidden, n_hidden)
        self.linear_time_2 = nn.Linear(n_hidden, n_hidden)
        self.linear_hidden_2_1 = nn.Linear(n_hidden, n_hidden)
        self.linear_hidden_2_2 = nn.Linear(n_hidden, n_hidden)
        self.linear_output = nn.Linear(n_hidden, n_out)

    def forward(self, xt: torch.Tensor, time: torch.Tensor, label: torch.Tensor) -> torch.Tensor:
        
        c = self.label_emb(label)
        
        x = torch.cat([xt,c],1)
        
        x = self.linear_input(x)

        h=self._activation(x)
        h=self.linear_hidden_1_1(h)
        h=h+self.linear_time_1(time)
        h=self._activation(h)
        h=self.linear_hidden_1_2(h)
        x=x+h

        h=self._activation(x)
        h=self.linear_hidden_2_1(h)
        h=h+self.linear_time_2(time)
        h=self._activation(h)
        h=self.linear_hidden_2_2(h)
        x=x+h



        #for _ in range(self._n_blocks):
        #    h = self._activation(x)
        #    h = self.linear_hidden(h)
        #    h += self.linear_time(time)
        #    h = self._activation(h)
        #    h = self.linear_hidden(h)
        #    x += h

        outputs = self.linear_output(x)

        return outputs

@dataclasses.dataclass
class NetConfig:
    resnet_n_blocks: int = 2
    resnet_n_hidden: int = 256
    resnet_n_out: int = 6
    activation: str = 'elu'
    time_embedding_dim: int = 256


class Net(nn.Module):
    """Combines MLP and time embeddings."""
    def __init__(self, net_config: NetConfig, name: str = None):
        super(Net, self).__init__()

        self._time_encoder = SinusoidalTimeEmbedding(net_config.time_embedding_dim)
        self._predictor = ResidualMLP(
            n_blocks=net_config.resnet_n_blocks,
            n_hidden=net_config.resnet_n_hidden,
            n_out=net_config.resnet_n_out,
            activation=net_config.activation
        )

    def forward(self, noisy_data: torch.Tensor, time: torch.Tensor, label: torch.Tensor) -> torch.Tensor:
        time_embedding = self._time_encoder(time)
        outputs = self._predictor(noisy_data, time_embedding, label)
        return outputs

class DiffusionModel(nn.Module):
    """Diffusion model."""

    def __init__(self, diffusion_process, time_sampler, net_config, data_shape):
        super(DiffusionModel, self).__init__()

        self._process = diffusion_process
        self._time_sampler = time_sampler
        self._net_config = net_config
        self._data_shape = data_shape
        self.net_fwd = Net(net_config)

    #@functools.partial(torch.jit.script_method)
    def loss(self, x0: torch.Tensor, label: torch.Tensor) -> torch.Tensor:
        """Computes MSE between the true noise and predicted noise,
        i.e. the goal of the network is to correctly predict eps from a noisy observation
        xt = alpha(t) * x0 + sigma(t)**2 * eps"""

        t = self._time_sampler.sample( shape=(x0.shape[0],))  # sample time

        eps = torch.randn_like(x0, device=x0.device)  # sample noise

        xt = self._process.sample(x0, t, eps)  # corrupt the data

        net_outputs = self.net_fwd(xt, t, label)  # get net outputs

        loss = torch.mean((net_outputs - eps) ** 2)  # compute MSE loss between predicted and true noise

        return loss

    #@functools.partial(torch.jit.script_method)
    def loss_per_timesteps(self,  x0: torch.Tensor, eps: torch.Tensor, timesteps: torch.Tensor, label: torch.Tensor) -> torch.Tensor:
        """ Computes loss values at given timesteps."""
        losses = []
        for t in timesteps:
            t = int(t.item()) * torch.ones((x0.shape[0],), dtype=torch.int32, device=x0.device)
            xt = self._process.sample(x0, t, eps)
            net_outputs = self.net_fwd(xt, t, label)
            loss = torch.mean((net_outputs - eps) ** 2)
            losses.append(loss)
        return torch.stack(losses)

    #@functools.partial(torch.jit.script_method)
    def _reverse_process_step(
        self,
        xt: torch.Tensor,
        t: int,
        label: torch.Tensor
    ) -> Tuple[torch.Tensor, torch.Tensor]:
        """Computes parameters of a Gaussian p(x_{t-1}| x_t, x0_pred)."""
        t = t * torch.ones((xt.shape[0],), dtype=torch.int32, device=xt.device)

        eps_pred = self.net_fwd(xt, t, label)  # predict epsilon from x_t

        sqrt_a_t = self._process.alpha(t) / self._process.alpha(t - 1)
        inv_sqrt_a_t = bcast_right(1.0 / sqrt_a_t, xt.ndim)
        inv_sqrt_a_t=inv_sqrt_a_t.to(device)

        beta_t = 1.0 - sqrt_a_t ** 2
        beta_t = bcast_right(beta_t, xt.ndim)
        beta_t=beta_t.to(device)

        inv_sigma_t = bcast_right(1.0 / self._process.sigma(t), xt.ndim)
        inv_sigma_t=inv_sigma_t.to(device)

        mean = inv_sqrt_a_t * (xt.to(device) - beta_t * inv_sigma_t * eps_pred.to(device))

        # DDPM instructs to use either the variance of the forward process
        # or the variance of q(x_{t-1}|x_t, x_0). Former is easier.
        std = torch.sqrt(beta_t)

        z = torch.randn_like(xt)

        return mean + std * z


    def sample(self, x0, sample_size, label):
        with torch.no_grad():
            x = torch.randn((sample_size,) + self._data_shape, device=x0.device)

            for t in range(self._process.tmax, 0, -1):
                x = self._reverse_process_step( x, t, label)

        return x

# create the model
diffusion_process = DiscreteDDPMProcess(num_diffusion_timesteps=1000)
time_sampler = UniformDiscreteTimeSampler(diffusion_process.tmin, diffusion_process.tmax)
model = DiffusionModel(diffusion_process, time_sampler, net_config=NetConfig(), data_shape=(6,))

x0_batch=torch.tensor(np.array([1])).to(device)

label_to_num = {
    "EUR": 0,
    "AFR": 1,
    "EAS": 2,
}

df=np.load("/home/tau/aszatkow/work/data/65k_all_labels.npy",allow_pickle=True)  
#corresponds to 1000G_chr1_first_65535.recode.hapt file with intra-continental population label as first column
#super-population labels as second column and sample id (eg "HG00096") as third column, followed by 65535 remaining columns
df=pd.DataFrame(df)
df=df.loc[(df[1].isin(["EUR","AFR","EAS"]))].values #keep only EUR, AFR, EAS
idx_to_keep=np.unique(df[:,0])[2:]
print(df.shape)
df=df[np.where(np.isin(df[:, 0], idx_to_keep))[0]] #Remove ACB and ASW
print(df.shape) #here there are 3020 samples

pair_idx=[(i,i+1) for i in range(0,df.shape[0],2)] #when shuffling conserve the haplotypes pair representing one individual
np.random.seed(42)
np.random.shuffle(pair_idx)

shuffled_idx=flatten(pair_idx)

df=df[shuffled_idx]

df_train=df[:2416] #2416 corresponds to 80% of 3020
#at this stage df_train has EUR(820, 65538) AFR(792, 65538) EAS(804, 65538)

df_train_copy=df_train.copy() #new for _differentRDMsplit_v2

idx=pd.read_csv("../generated/REAL_TRAIN_80%_EUR_AFR_EAS_without_ACB_ASW_3pop_1188train.tsv",header=None).values.reshape(-1,) #UNNECESSARY FILE

df_train=df_train[np.where(np.isin(df_train[:, 2], idx))[0]] #samples are the same so it's should not be doing anything. CAN BE REMOVED

df_train=df_train[236:] #we want the training set of the generative model to be exactly the one seen by LAI-Net. LAI-Net takes df_train (not the df_train[236:] one) and applies a train-validation split of 90-10%. df_train[236:] corresponds to the 90%


###CREATING THE MVN###
print(0)
pca=PCA()#n_components=6)
proj=pca.fit_transform(df_train[:,3:])

proj_real_low_freq=proj[:,6:] #select low variance modes (name is incorrect since low variance modes are high frequency modes)
del proj
proj_real_low_freq=proj_real_low_freq.astype(float)

mean_vector = np.mean(proj_real_low_freq, axis=0)
cov_matrix = np.cov(proj_real_low_freq, rowvar=False)
print(3)
# Create a multivariate Gaussian model
from scipy.stats import multivariate_normal
gaussian_model = multivariate_normal(mean=mean_vector, cov=cov_matrix,allow_singular=True)
del mean_vector
del cov_matrix
del df_train
###CREATING THE MVN###

LEGEND_PATH="/home/tau/aszatkow/work/data/1000G_chr1_first_65535.recode.legend"
real_train=False
nSNP='65k'
pop=['AFR','EUR','EAS']
npop=len(pop)

encode_label_dic={"AFR":0,"EAS":1,"EUR":2}

mult=4 #we generate synthetic sample size 4x larger than the real dataset
N=2376*mult
N_pop=N//3
N_pop, N_pop//2

for frac in [1,5,10,25,50,75,100]: #frac of train to keep (+5)
    print("-"*10+f"frac={frac}"+"-"*10)
    frac_train=int(np.rint(frac*2416/100)) #applied on 2416 sequences and not 2416-236
    frac_train_per_pop = int(np.floor(frac_train/3))
    if frac_train_per_pop % 2 != 0: frac_train_per_pop +=1 #needs to be even otherwise it's a mess

    #where accuracy will be saved
    f=open(f"/home/tau/aszatkow/work/crete_maiwen_titouan/ter-bioinfo/DATA_AUG_PCA-DDPM_bootstrap_mult={mult}_TRAIN_frac_size={frac}%_multipleITER_differentRDMsplit_v2.txt","a")
    for i in range(1,6):
        #loading a diffusion model
        model=torch.load(f"/home/tau/aszatkow/work/crete_maiwen_titouan/DGE/run_02/gen_models/conditional_PCA_DDPM_6Comp_train-EUR_EAS_AFR_without_ACB_ASW-30Ksteps_REAL_TRAIN_80%_LAINET_PROCESS_2140hapseq_nITER_{i}.pth").to(device)

        #generate AGs from the sources pop in latent space
        lst=[]
        n_samples=N_pop
        for pop_label in list(label_to_num):
            label=(torch.ones(n_samples)*label_to_num[pop_label]).to(torch.int64).to(device)
            samples = model.sample(x0_batch,n_samples,label)
            lst.append(samples)
            print(pop_label)
        samples=torch.concat(lst)
        del lst
        samples=samples.detach().cpu().numpy()
        del model

        #invert them back in snp space (START)
        print(4)
        fake_MVgaussian=gaussian_model.rvs(size=N)
        print(5)
        AGs=np.hstack((samples,fake_MVgaussian))
        del samples
        del fake_MVgaussian
        inv_transform_EUR_AFR_EAS=pca.inverse_transform(AGs)
        inv_transform_EUR_AFR_EAS[inv_transform_EUR_AFR_EAS<0.5]=0
        inv_transform_EUR_AFR_EAS[inv_transform_EUR_AFR_EAS>=0.5]=1
        df_syn=inv_transform_EUR_AFR_EAS.astype(int)
        del inv_transform_EUR_AFR_EAS
        #invert them back in snp space (END)
        
        df_train=df_train_copy #this is df_train[:2416] of the beginning
        
        #this is just a shuffling of real data (start)
        pair_idx=[(i,i+1) for i in range(0,df_train.shape[0],2)] #NEW
        np.random.seed(i+50) #NEW
        np.random.shuffle(pair_idx)#NEW

        shuffled_idx=flatten(pair_idx)#NEW

        df_train=df_train[shuffled_idx]#NEW
        print(frac, i, df_train.shape)
        #this is just a shuffling of real data (end)
        
        #selecting frac_train_per_pop samples from each continents
        np.random.seed(frac+i)
        idx_EUR=np.random.choice(np.where(df_train[:,1]=="EUR")[0],frac_train_per_pop)
        EUR=df_train[idx_EUR]
        
        np.random.seed(frac+i)
        idx_AFR=np.random.choice(np.where(df_train[:,1]=="AFR")[0],frac_train_per_pop)
        AFR=df_train[idx_AFR]
        
        np.random.seed(frac+i)
        idx_EAS=np.random.choice(np.where(df_train[:,1]=="EAS")[0],frac_train_per_pop)
        EAS=df_train[idx_EAS]
        
        df_train=np.vstack((EUR,AFR)) #used to be named "df"
        df_train=np.vstack((df_train,EAS))

        #merge syn with real
        snp_merge_real_syn=np.vstack((df_train[:,3:].astype(int),df_syn))

        #useful for shuffling the merged snp matrix stacked with pop lab as first col
        population_label_real=df_train[:,1] #2416 haplotypes total
        #array(['AFR', 'EAS', 'EUR']), array([792, 804, 820]) #haplotypes per pop
        population_label_syn=np.array(["EUR" for i in range(N_pop)]+["AFR" for i in range(N_pop)]+["EAS" for i in range(N_pop)]).reshape(-1,1)
        #increase of 3168 synth haplotypes per pop
        population_label_merge_real_syn=np.array(list(population_label_real)+list(population_label_syn.reshape(-1,len(population_label_syn))[0]))

        snp_merge_real_syn=np.hstack((population_label_merge_real_syn.reshape(-1,1),snp_merge_real_syn))

        pair_idx=[(i,i+1) for i in range(0,snp_merge_real_syn.shape[0],2)]
        np.random.seed(42)
        np.random.shuffle(pair_idx)

        shuffled_idx=flatten(pair_idx)

        snp_merge_real_syn=snp_merge_real_syn[shuffled_idx]
        population_label_merge_real_syn=np.array([snp_merge_real_syn[x,0] for x in np.arange(0,len(snp_merge_real_syn[:,0]),2)])

        sex=np.array([1 for i in range((N+df_train.shape[0])//2)]).reshape(-1,1)

        idx=np.array([f"AG{i}" for i in range(0, N+df_train.shape[0], 2)]).reshape(-1,1)

        snp_merge_real_syn = snp_merge_real_syn[:,1:].transpose() # .hap is, simply put, the transpose of our .hapt data
        snp_merge_real_syn = pd.DataFrame(snp_merge_real_syn)

        general_name=f"PCA-DDPM_TRAIN_VARIOUS_SIZES_DATA_AUG_{mult}TIMESBIGGERhapseq_EUR_AFR_EAS_without_ACB_ASW_BOOTSTRAP_nITER{i}_differentRDMsplit_v2"
        snp_merge_real_syn.to_csv("../data/input/artificial/65k_SNP/DATA_AUG_BOOTSTRAP_PCA-DDPM/" + general_name + ".hap", header=False, index=False, sep=" ") #writing SNP matrix of real augmented with synthetic
        del snp_merge_real_syn
        del df_syn
        tmp=np.hstack((np.hstack((idx,population_label_merge_real_syn.reshape(-1,1))),sex))
        general_name_smap=f"DATA_AUG_TRAIN_VARIOUS_SIZES_REAL_GENERIC_SYN_{mult}TIMESBIGGER_EUR_AFR_EAS_without_ACB_ASW_BOOTSTRAP_nITER{i}_differentRDMsplit_v2"
        tmp=pd.DataFrame(tmp,columns=["Sample","group","sex"])
        tmp.to_csv("../generated/DATA_AUG_BOOTSTRAP_PCA-DDPM/" + general_name_smap + ".sample", index=False, sep=" ") #necessary file for LAI-NET, columns: sample id, ancestry label, sex = constant = 1
        smap=tmp.drop("sex",axis=1)
        smap.rename({"group":"Superpopulation code"})
        smap.to_csv("../data/input/artificial/65k_SNP/DATA_AUG_BOOTSTRAP_PCA-DDPM/" + general_name_smap + ".smap", sep=" ", index=False, header=False)

        #convert hap to vcf
        GENERAL_PATH="/home/tau/aszatkow/work/crete_maiwen_titouan/ter-bioinfo/data/input/artificial/65k_SNP/DATA_AUG_BOOTSTRAP_PCA-DDPM/"
        SAMPLE_PATH="../generated/DATA_AUG_BOOTSTRAP_PCA-DDPM/" + general_name_smap + ".sample"
        OUTPUT_PATH=GENERAL_PATH+"reference_"+general_name+".vcf"
        HAP_PATH = GENERAL_PATH+general_name+".hap"

        bashCommand = f"bcftools convert --haplegendsample2vcf {HAP_PATH},{LEGEND_PATH},{SAMPLE_PATH} -o {OUTPUT_PATH}"
        subprocess.run(bashCommand, shell=True, env={'PATH': '/home/tau/aszatkow/miniforge3/envs/LAI/bin:' + os.environ['PATH']})
        os.system("gzip {}".format(OUTPUT_PATH))
        os.system("rm {}".format(HAP_PATH))

        reference_file_s=f"{OUTPUT_PATH}.gz"
        reference_map_file_s="../data/input/artificial/65k_SNP/DATA_AUG_BOOTSTRAP_PCA-DDPM/" + general_name_smap + ".smap"

        output_basename=f"../data/output_lainet/65k_SNP/DATA_AUG_BOOTSTRAP_PCA-DDPM/DATA_AUG_TRAIN_VARIOUS_SIZES_SYN_{mult}TIMESBIGGER_EUR_AFR_EAS_without_ACB_ASW_PCA-DDPM_BOOTSTRAP_nITER{i}/"
        os.makedirs(output_basename,exist_ok=True)

        train_file=f"../data/input/artificial/65k_SNP/DATA_AUG_BOOTSTRAP_PCA-DDPM/PCA-DDPM_TRAIN_VARIOUS_SIZES_DATA_AUG_{mult}TIMESBIGGERhapseq_EUR_AFR_EAS_without_ACB_ASW_BOOTSTRAP_nITER{i}_{N_pop//2}+396ind_differentRDMsplit_v2.vcf.gz"
        train_list_file=f"../data/input/artificial/65k_SNP/DATA_AUG_BOOTSTRAP_PCA-DDPM/PCA-DDPM_TRAIN_VARIOUS_SIZES_DATA_AUG_{mult}TIMESBIGGERhapseq_EUR_AFR_EAS_without_ACB_ASW_BOOTSTRAP_nITER{i}_{N_pop//2}+396ind_differentRDMsplit_v2.tsv"
        train_map_file=f"../data/input/artificial/65k_SNP/DATA_AUG_BOOTSTRAP_PCA-DDPM/PCA-DDPM_TRAIN_VARIOUS_SIZES_DATA_AUG_{mult}TIMESBIGGERhapseq_EUR_AFR_EAS_without_ACB_ASW_BOOTSTRAP_nITER{i}_{N_pop//2}+396ind_differentRDMsplit_v2.smap"
        data_folder_s=f"../data/input/artificial/65k_SNP/DATA_AUG_BOOTSTRAP_PCA-DDPM/"

        nsamplepop=(N_pop//2)+(frac_train_per_pop//2) #n_pop_min is for real data

        extract_from_file_antoine(reference_file_s,reference_map_file_s,nsamplepop,train_map_file,train_list_file,train_file,pop) #will create train_file, train_list_file and train_map_file
        #this function is to be find here: https://github.com/MaiDeme/LAI-Net/blob/main/utils/format_file.py

        os.system("rm {}".format(reference_file_s))

        chm              = 1
        #output_basename  = output_name('lainet',btrain,nsamplepop*5,btest,nquerypop*5)
        if (os.path.isdir(output_basename)==False):
            print('Creating directory...')
            os.makedirs(output_basename,exist_ok=True)
            print('Done')

        # Load vcf and map files and config
        config = yaml.load(open(config_path), Loader=yaml.FullLoader)

        print(config)

        #%time
        ## Training
        train_main(config, train_file, train_map_file, output_basename,chm,genetic_map_file)

        print("@"*20)
        f.write("@"*20+"\n")
        print("MODEL EVALUATION")
        f.write("MODEL EVALUATION"+"\n")
        for n_gen in [30,60]:
            test_file=f"../data/input/real/65k_SNP/REAL_VAL_20%_EUR_AFR_EAS_without_ACB_ASW_{n_gen}_gen.vcf"
            #%time
            ## Inference
            net, predicted, probs, val_snps = inference_main(config, test_file, output_basename, output_basename+f"query_test_{n_gen}_gen")

            print('Plotting results!')
            output_file = output_basename+f"query_test_{n_gen}_gen."+"msp.tsv"

            msp_df = pd.read_csv(output_file, sep="\t",header=1)
            msp_df

            df_test=readVCF(test_file)
            df_test=df_test.replace(encode_label_dic)

            conf_matrix = confusion_matrix(df_test.values[:,6:].astype(int).flatten('F'), msp_df.values[:,6:].flatten('F'))

            print("$"*3,f"BOOTSTRAP nITER{i}, Number of generations : {n_gen}")
            f.write("$"*3+f"BOOTSTRAP nITER{i}, Number of generations : {n_gen}"+"\n")
            #compute precision
            for j in range(3):
                print(list(encode_label_dic)[j],":",conf_matrix[j,j]/conf_matrix[:,j].sum()*100)
                f.write(list(encode_label_dic)[j]+":"+f"{conf_matrix[j,j]/conf_matrix[:,j].sum()*100}"+"\n")
    f.close()
