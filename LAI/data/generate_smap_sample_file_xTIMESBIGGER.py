import numpy as np
import pandas as pd

#2376 corresponds to 792*3

mul=1
N=2376*mul
N_pop=N//3
N_pop, N_pop//2


sex=np.array([1 for i in range(N//2)]).reshape(-1,1)
idx=np.array([f"AG{i}" for i in range(0, N, 2)]).reshape(-1,1)
population_label=np.array(["EUR" for i in range(N_pop//2)]+["AFR" for i in range(N_pop//2)]+["EAS" for i in range(N_pop//2)]).reshape(-1,1)
tmp=np.hstack((np.hstack((idx,population_label)),sex))

if mul==1:general_name="SYN_TRAIN_80%_2376_EUR_AFR_EAS_without_ACB_ASW"
else: general_name=f"SYN_TRAIN_{mul}TIMESBIGGER_EUR_AFR_EAS_without_ACB_ASW"

tmp=pd.DataFrame(tmp,columns=["Sample","group","sex"])
tmp.to_csv("./generated/" + general_name + ".sample", index=False, sep=" ")

smap=tmp.drop("sex",axis=1)
smap.rename({"group":"Superpopulation code"})
smap.to_csv("./data/input/artificial/65k_SNP/" + general_name + ".smap", sep=" ", index=False, header=False)