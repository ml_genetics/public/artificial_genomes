to simulate recombination using haptools

`haptools simgenotype --model model_<num_generations>.data --mapdir map/ --<input>.vcf --sample_info <input>.smap --pop_field --sample_field --chroms 1 --out ./<output>.vcf`

`<input>.{vcf,smap}` is `REAL_VAL_20%_EUR_AFR_EAS_without_ACB_ASW.{vcf,smap}` produced as output by `make_train_test_three_continent_data.py`

`<output>.vcf` is `REAL_VAL_20%_EUR_AFR_EAS_without_ACB_ASW_<num_generations>_gen.vcf.gz` in `..`

`--mapdir` takes path to folder (here `map/`) containing the genetic map of the desired chromosome (here chr1)

