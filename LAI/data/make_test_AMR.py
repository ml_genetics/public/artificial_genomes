import numpy as np
import pandas as pd

df=np.load("/home/antoine/work/data/65k_real_all_labels.npy",allow_pickle=True)
#corresponds to 1000G_chr1_first_65535.recode.hapt file with intra-continental population label as first column
#super-population labels as second column and sample id (eg "HG00096") as third column, followed by 65535 remaining columns
idx_to_keep=np.unique(df[:,0])[:2] #keep ASW and ACB
df_subsetAFR=df[np.where(np.isin(df[:, 0], idx_to_keep))[0]] #314 haplotypes

df=pd.DataFrame(df)
df=df.loc[(df[1].isin(["AMR"]))].values # 694 haplotypes

df_test=np.vstack((df_subsetAFR,df))
del df_subsetAFR
del df

pair_idx_test=[(i,i+1) for i in range(0,df_test.shape[0],2)] #when shuffling conserve the haplotypes pair representing one individual
np.random.shuffle(pair_idx_test)
shuffled_idx_test=flatten(pair_idx_test)
df_test=df_test[shuffled_idx_test] #1008 haplotypes

tmp=df_test[[i for i in range(0, df_test.shape[0], 2)]]
samples_df_=pd.DataFrame(np.vstack((tmp[:,2],tmp[:,1])).T,columns=["Sample","group"])
samples_df_["sex"]=1
del tmp

samples_df_.to_csv("./generated/" + "REAL_AMR_with_ACB_ASW" + ".sample", index=False, sep=" ")

df_test=df_test[:,3:].astype(int).T
df_test=pd.DataFrame(df_test)

df_test.to_csv("./generated/" + "REAL_AMR_with_ACB_ASW" + ".hap", header=False, index=False, sep=" ")

samples_df_=samples_df_.drop(columns=['sex'])
samples_df_=samples_df_.rename(columns={"group":"Superpopulation code"})

samples_df_.to_csv("./generated/" + "REAL_AMR_with_ACB_ASW" + ".smap", sep=" ", index=False, header=False)
