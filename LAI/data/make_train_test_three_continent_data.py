from __future__ import print_function
#%matplotlib inline
import argparse
import os
import random
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from IPython.display import HTML
import gc
import seaborn as sns

import dataclasses
from typing import Sequence
import functools
from typing import Tuple  # Add this line to import Tuple
from torch import optim
import pytorch_warmup as warmup

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn import preprocessing

import pandas as pd
import numpy as np

def flatten(xss):
    return [x for xs in xss for x in xs]

#import ot

# Decide which device we want to run on
ngpu=1
device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")

if device.type == 'cuda':
    print(torch.cuda.get_device_name(0))
    print('Memory Usage:')
    print('Allocated:', round(torch.cuda.memory_allocated(0)/1024**3,1), 'GB')
    print('Cached:   ', round(torch.cuda.memory_reserved(0)/1024**3,1), 'GB')

df=np.load("/home/antoine/work/data/65k_real_all_labels.npy",allow_pickle=True)
#corresponds to 1000G_chr1_first_65535.recode.hapt file with intra-continental population label as first column
#super-population labels as second column and sample id (eg "HG00096") as third column, followed by 65535 remaining columns
df=pd.DataFrame(df)
df=df.loc[(df[1].isin(["EUR","AFR","EAS"]))].values #keep only EUR, AFR, EAS
idx_to_keep=np.unique(df[:,0])[2:]
print(df.shape)
df=df[np.where(np.isin(df[:, 0], idx_to_keep))[0]] #Remove ACB and ASW
print(df.shape)

pair_idx=[(i,i+1) for i in range(0,df.shape[0],2)] #when shuffling conserve the haplotypes pair representing one individual
np.random.seed(42)
np.random.shuffle(pair_idx)
shuffled_idx=flatten(pair_idx)
df=df[shuffled_idx

df_train=df[:2416]#2416 corresponds to 80% of 3020
#at this stage df_train has EUR(820, 65538) AFR(792, 65538) EAS(804, 65538)
df_val=df[2416:]

tmp_train=df_train[[i for i in range(0, df_train.shape[0], 2)]]
tmp_val=df_val[[i for i in range(0, df_val.shape[0], 2)]]

samples_df_train=pd.DataFrame(np.vstack((tmp_train[:,2],tmp_train[:,1])).T,columns=["Sample","group"])
samples_df_train["sex"]=1
samples_df_val=pd.DataFrame(np.vstack((tmp_val[:,2],tmp_val[:,1])).T,columns=["Sample","group"])
samples_df_val["sex"]=1
del tmp_train
del tmp_val

samples_df_train.to_csv("./generated/" + "REAL_TRAIN_80%_EUR_AFR_EAS_without_ACB_ASW" + ".sample", index=False, sep=" ")
samples_df_val.to_csv("./generated/" + "REAL_VAL_20%_EUR_AFR_EAS_without_ACB_ASW" + ".sample", index=False, sep=" ")

del df

df_train=df_train[:,3:].astype(int).T
df_val=df_val[:,3:].astype(int).T

df_train=pd.DataFrame(df_train)
df_val=pd.DataFrame(df_val)

df_train.to_csv("./generated/" + "REAL_TRAIN_80%_EUR_AFR_EAS_without_ACB_ASW" + ".hap", header=False, index=False, sep=" ")
df_val.to_csv("./generated/" + "REAL_VAL_20%_EUR_AFR_EAS_without_ACB_ASW" + ".hap", header=False, index=False, sep=" ")

samples_df_train=samples_df_train.drop(columns=['sex'])
samples_df_train=samples_df_train.rename(columns={"group":"Superpopulation code"})

samples_df_val=samples_df_val.drop(columns=['sex'])
samples_df_val=samples_df_val.rename(columns={"group":"Superpopulation code"})

samples_df_train.to_csv("./generated/" + "REAL_TRAIN_80%_EUR_AFR_EAS_without_ACB_ASW" + ".smap", sep=" ", index=False, header=False)
samples_df_val.to_csv("./generated/" + "REAL_VAL_20%_EUR_AFR_EAS_without_ACB_ASW" + ".smap", sep=" ", index=False, header=False)