print('Start importing required libraries...')
import os, sys, time
import subprocess
from tqdm.auto import tqdm
import allel
import yaml
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import torch
torch.autograd.set_detect_anomaly(True)
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import math
from collections import Counter
import gzip
#import tagore
from scipy.interpolate import interp1d

import random

sys.path.append('/home/tau/aszatkow/work/crete_maiwen_titouan/ter-bioinfo/LAI-Net')

from lainet.models.network_constructor import get_network
from lainet.utils.eval import compute_accuracy, AccuracyLogger, complete_sk_eval, print_sk_eval
from lainet.utils.reader import load_founders_from_vcf_and_map, load_results_file
from lainet.utils.output_writer import get_meta_data, write_msp_tsv
from lainet.training import train_main, eval_predictions
from lainet.inference import inference_main 

from utils.format_file import add_result, naming_file, output_name
from utils.format_file import naming_file, extract_from_same_file, extract_from_file,output_name_mixed

import argparse
import subprocess
sys.path.remove('/home/tau/aszatkow/work/crete_maiwen_titouan/ter-bioinfo/LAI-Net')
sys.path.append('/home/tau/aszatkow/work/crete_maiwen_titouan/ter-bioinfo/')

print('sys.argv[0] =', sys.argv[0])             
pathname = os.path.dirname(sys.argv[0])        
print('path =', pathname)
print('full path =', os.path.abspath(pathname)) 

print('Done importing')

parser = argparse.ArgumentParser(description='Generate Artificial Genomes from PCA-DDPM')
parser.add_argument('--idx_run', type=str, help='Index of run (type==str); eg. run_01, run_02 ...')
args = parser.parse_args()

idx_run=args.idx_run

GENERAL_PATH="/home/tau/aszatkow/work/crete_maiwen_titouan/"
genetic_map_file =  "../ter-bioinfo/data/input/real/allchrs_b37.gmap"
config_path = '../ter-bioinfo/LAI-Net/configs/default.yaml'
reference_map_file_s="../ter-bioinfo/data/input/artificial/65k_SNP/SYN_TRAIN_80%_2376_EUR_AFR_EAS_without_ACB_ASW.smap"
chm = 1
config = yaml.load(open(config_path), Loader=yaml.FullLoader)

reference_file_s=f"{GENERAL_PATH}DGE/naive_run_{idx_run}/data/reference_AGs_conditional_PCA_DDPM_6Comp_train-EUR_EAS_AFR_without_ACB_ASW-30Ksteps_REAL_TRAIN_80%_LAINET_PROCESS_2140hapseq.vcf.gz"

output_basename=f"{GENERAL_PATH}DGE/naive_run_{idx_run}/lainet_models_and_results/"
os.makedirs(output_basename,exist_ok=True)

train_file=f"{GENERAL_PATH}DGE/naive_run_{idx_run}/data/AGs_conditional_PCA_DDPM_6Comp_train-EUR_EAS_AFR_without_ACB_ASW-30Ksteps_REAL_TRAIN_80%_LAINET_PROCESS_2140hapseq_396ind.vcf.gz"
train_list_file=f"{GENERAL_PATH}DGE/naive_run_{idx_run}/data/AGs_conditional_PCA_DDPM_6Comp_train-EUR_EAS_AFR_without_ACB_ASW-30Ksteps_REAL_TRAIN_80%_LAINET_PROCESS_2140hapseq_396ind.tsv"
train_map_file=f"{GENERAL_PATH}DGE/naive_run_{idx_run}/data/AGs_conditional_PCA_DDPM_6Comp_train-EUR_EAS_AFR_without_ACB_ASW-30Ksteps_REAL_TRAIN_80%_LAINET_PROCESS_2140hapseq_396ind.smap"
data_folder_s=f"{GENERAL_PATH}DGE/naive_run_{idx_run}/data/"

#os.system("rm {}".format(reference_file_s))

nsamplepop=396
real_train=False
nSNP='65k'
pop=['AFR','EUR','EAS']
npop=len(pop)

extract_from_file(reference_file_s,reference_map_file_s,nsamplepop,train_map_file,train_list_file,train_file,pop)
train_main(config, train_file, train_map_file, output_basename,chm,genetic_map_file)

test_file="../ter-bioinfo/data/input/real/65k_SNP/REAL_VAL_20%_EUR_AFR_EAS_without_ACB_ASW_30_gen.vcf"
net, predicted, probs, val_snps = inference_main(config, test_file, output_basename, output_basename+"query_test_30_gen")

test_file="../ter-bioinfo/data/input/real/65k_SNP/REAL_VAL_20%_EUR_AFR_EAS_without_ACB_ASW_60_gen.vcf"
net, predicted, probs, val_snps = inference_main(config, test_file, output_basename, output_basename+"query_test_60_gen")

os.system("rm {}".format(reference_file_s))