print('Start importing required libraries...')
import os, sys, time
import subprocess
from tqdm.auto import tqdm
import allel
import yaml
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import torch
torch.autograd.set_detect_anomaly(True)
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import math
from collections import Counter
import gzip
#import tagore
from scipy.interpolate import interp1d

import random

sys.path.append('/home/tau/aszatkow/work/crete_maiwen_titouan/ter-bioinfo/LAI-Net')

from lainet.models.network_constructor import get_network
from lainet.utils.eval import compute_accuracy, AccuracyLogger, complete_sk_eval, print_sk_eval
from lainet.utils.reader import load_founders_from_vcf_and_map, load_results_file
from lainet.utils.output_writer import get_meta_data, write_msp_tsv
from lainet.training import train_main, eval_predictions
from lainet.inference import inference_main 

from sklearn.metrics import confusion_matrix
from lainet.visualization import plot_cm

from utils.format_file import add_result, naming_file, output_name
from utils.format_file import naming_file, extract_from_same_file, extract_from_file,output_name_mixed

import argparse
import subprocess
sys.path.remove('/home/tau/aszatkow/work/crete_maiwen_titouan/ter-bioinfo/LAI-Net')
sys.path.append('/home/tau/aszatkow/work/crete_maiwen_titouan/ter-bioinfo/')

import argparse
import subprocess

print('Done importing')

def readVCF(filename,n_snps=500):
    f = open(filename, "r")
    columns=["chm", "spos", "epos", "sgpos", "egpos", "n snps"]
    df = pd.DataFrame(columns=["chm", "spos", "epos", "sgpos", "egpos", "n snps"])
    currSNP=0
    spos=-1
    ancestryCount = []
    stupidL=[]
    epos=-1
    for line in f:

        if line[0] == "#" and line[1] == "#":
            pass
        elif line[0] == "#" and line[1] == "C":
            curr = line.split()
            for elt in curr[9:]:
                columns.append(f'{elt}.0')
                columns.append(f'{elt}.1')
                ancestryCount.append({})
                ancestryCount.append({})
                stupidL.append(f'{elt}.0')
                stupidL.append(f'{elt}.1')
            df = pd.DataFrame(columns=columns)
        else:
            curr = line.split()
            if currSNP==0:
                spos=curr[1]
            if currSNP%n_snps==0 and currSNP>0:
                newrow={"chm":curr[0],"spos":spos,"epos":epos,"sgpos":1, "egpos":1,"n snps":n_snps}
                for j in range(9,len(curr)):
                    for i in range(2):
                        max=0
                        ind=""
                        for key in ancestryCount[(j-9)*2+i]:
                            if ancestryCount[(j-9)*2+i][key]>max:
                                max=ancestryCount[(j-9)*2+i][key]
                                ind=key
                        ancestryCount[(j-9)*2+i]={}
                        newrow[stupidL[(j-9)*2+i]]=key
                spos=curr[1]
                df.loc[len(df)]=newrow
                #df.append(newrow, ignore_index=True)
            for j in range(9,len(curr)):
                anc=curr[j].split(":")[1].split(",")
                for i in range(2):

                    if anc[i] in ancestryCount[(j-9)*2+i]:
                        ancestryCount[(j-9)*2+i][anc[i]]+=1
                    else:
                        ancestryCount[(j-9)*2+i][anc[i]]=1
            currSNP+=1
            epos=curr[1]
            
    df.loc[len(df)-1,"epos"]=curr[1]
    
    return df

parser = argparse.ArgumentParser(description='Generate Artificial Genomes from PCA-DDPM')
parser.add_argument('--idx_run', type=str, help='Index of run (type==str); eg. run_01, run_02 ...')
args = parser.parse_args()

idx_run=args.idx_run

GENERAL_PATH="/home/tau/aszatkow/work/crete_maiwen_titouan/"
test_file_30_gen="/home/tau/aszatkow/work/crete_maiwen_titouan/ter-bioinfo/data/input/real/65k_SNP/REAL_VAL_20%_EUR_AFR_EAS_without_ACB_ASW_30_gen.vcf"
test_file_60_gen="/home/tau/aszatkow/work/crete_maiwen_titouan/ter-bioinfo/data/input/real/65k_SNP/REAL_VAL_20%_EUR_AFR_EAS_without_ACB_ASW_60_gen.vcf"

encode_label_dic={"AFR":0,"EAS":1,"EUR":2}
df_30_gen=readVCF(test_file_30_gen)
df_60_gen=readVCF(test_file_60_gen)
df_30_gen=df_30_gen.replace(encode_label_dic)
df_60_gen=df_60_gen.replace(encode_label_dic)

f=open(f'{GENERAL_PATH}DGE/run_{idx_run}/results/res_DGE_BIG.txt','a')

lst_mean_AFR_30, lst_mean_EAS_30, lst_mean_EUR_30 = [], [], []
dic_30={0:lst_mean_AFR_30, 1: lst_mean_EAS_30, 2:lst_mean_EUR_30}

lst_mean_AFR_60, lst_mean_EAS_60, lst_mean_EUR_60 = [], [], []
dic_60={0:lst_mean_AFR_60, 1: lst_mean_EAS_60, 2:lst_mean_EUR_60}

for n_iter in range(1,6):
    output_basename=f"{GENERAL_PATH}DGE/run_{idx_run}/lainet_models_and_results/0{str(n_iter)}/"
    
    output_file_30 = output_basename+"query_test_30_gen_DGE_BIG."+"msp.tsv"
    msp_df_30 = pd.read_csv(output_file_30, sep="\t",header=1)
    conf_matrix_30 = confusion_matrix(df_30_gen.values[:,6:].astype(int).flatten('F'), msp_df_30.values[:,6:].flatten('F'))
    f.write(f"@run_{idx_run}#30_generations#lainet_model_0{str(n_iter)}"+'\n')
    for i in range(3):
        acc_30=np.round(conf_matrix_30[i,i]/conf_matrix_30[:,i].sum()*100,3)
        f.write(f"{acc_30}"+'\n')
        dic_30[i].append(acc_30)

f.write(f"@run_{idx_run}#30_generations#Average accuracy over K"+'\n')
f.write(f"AFR: {np.round(np.mean(dic_30[0]),3)}"+'\n')
f.write(f"EAS: {np.round(np.mean(dic_30[1]),3)}"+'\n')
f.write(f"EUR: {np.round(np.mean(dic_30[2]),3)}"+'\n')

f.write("\n")

for n_iter in range(1,6):
    output_basename=f"{GENERAL_PATH}DGE/run_{idx_run}/lainet_models_and_results/0{str(n_iter)}/"
    output_file_60 = output_basename+"query_test_60_gen_DGE_BIG."+"msp.tsv"
    msp_df_60 = pd.read_csv(output_file_60, sep="\t",header=1)
    conf_matrix_60 = confusion_matrix(df_60_gen.values[:,6:].astype(int).flatten('F'), msp_df_60.values[:,6:].flatten('F'))   
    f.write(f"@run_{idx_run}#60_generations#lainet_model_0{str(n_iter)}"+'\n')
    for i in range(3):
        acc_60=np.round(conf_matrix_60[i,i]/conf_matrix_60[:,i].sum()*100,3)
        f.write(f"{acc_60}"+'\n')
        dic_60[i].append(acc_60)
    
f.write(f"@run_{idx_run}#60_generations#Average accuracy over K"+'\n')
f.write(f"AFR: {np.round(np.mean(dic_60[0]),3)}"+'\n')
f.write(f"EAS: {np.round(np.mean(dic_60[1]),3)}"+'\n')
f.write(f"EUR: {np.round(np.mean(dic_60[2]),3)}"+'\n')
f.close()