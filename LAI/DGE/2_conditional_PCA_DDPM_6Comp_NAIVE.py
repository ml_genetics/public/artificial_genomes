from __future__ import print_function
#%matplotlib inline
import argparse
import os
import random
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from IPython.display import HTML
import gc
import seaborn as sns

import dataclasses
from typing import Sequence
import functools
from typing import Tuple  # Add this line to import Tuple
from torch import optim
import pytorch_warmup as warmup

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn import preprocessing

import pandas as pd
import numpy as np

import argparse

parser = argparse.ArgumentParser(description='Train PCA-DDPM for Deep Generative Ensemble')
parser.add_argument('--idx_run', type=str, help='Index of run (type==str); eg. run_01, run_02 ...')
args = parser.parse_args()

idx_run=args.idx_run

#import ot

# Decide which device we want to run on
ngpu=1
device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")

if device.type == 'cuda':
    print(torch.cuda.get_device_name(0))
    print('Memory Usage:')
    print('Allocated:', round(torch.cuda.memory_allocated(0)/1024**3,1), 'GB')
    print('Cached:   ', round(torch.cuda.memory_reserved(0)/1024**3,1), 'GB')
    
def bcast_right(x: torch.Tensor, ndim: int) -> torch.Tensor:
    """Util function for broadcasting to the right."""
    if x.ndim > ndim:
        raise ValueError(f'Cannot broadcast a value with {x.ndim} dims to {ndim} dims.')
    elif x.ndim < ndim:
        difference = ndim - x.ndim
        return x.view(x.shape + (1,) * difference)
    else:
        return x

from torch.utils.data import DataLoader, TensorDataset

def dataset_iterator(data: np.ndarray, batch_size: int, rng_seed: int = 42):
    """ PyTorch DataLoader iterator."""
    torch_data = torch.from_numpy(data)
    train_dataset = TensorDataset(torch_data)
    train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True)
    return train_loader#iter(train_loader)

#labelled_proj=np.load('/home/tau/aszatkow/work/data/EUR_AFR_EAS_proj_6Comp.npy',allow_pickle=True)
#labelled_proj=np.load('/home/tau/aszatkow/work/data/EUR_AFR_EAS_without_ACB_ASW_proj_6Comp.npy',allow_pickle=True)
labelled_proj=np.load('/home/tau/aszatkow/work/data/REAL_TRAIN_80%_LAINET_PROCESS_2140hapseq_EUR_AFR_EAS_without_ACB_ASW_proj_6Comp.npy',allow_pickle=True)

train_loader = dataset_iterator(labelled_proj, batch_size=1667)
train_data_iter = iter(train_loader)

class DiscreteDDPMProcess:
    """A Gaussian diffusion process: q(xt|x0) = N(alpha(t)*x0, sigma(t)^2 * I),
    which implies the following transition from x0 to xt:

    xt = alpha(t) x0 + sigma(t) eps, eps ~ N(0, I).

    Diffusion processes differ in how they specify alpha(t) and/or sigma(t).
    Here we follow the DDPM paper.

    """
    def __init__(
        self,
        num_diffusion_timesteps: int = 1000,
        beta_start: float = 0.0001,
        beta_end: float = 0.02,
    ):
        self._num_diffusion_timesteps = num_diffusion_timesteps
        self._beta_start = beta_start
        self._beta_end = beta_end
        self._betas = np.linspace(self._beta_start, self._beta_end, self._num_diffusion_timesteps)

        alphas_bar = self._get_alphas_bar()
        self._alphas = torch.tensor(np.sqrt(alphas_bar), dtype=torch.float32)
        self._sigmas = torch.tensor(np.sqrt(1 - alphas_bar), dtype=torch.float32)

    @property
    def tmin(self):
        return 1

    @property
    def tmax(self):
        return self._num_diffusion_timesteps

    def _get_alphas_bar(self) -> np.ndarray:
        alphas_bar = np.cumprod(1.0 - self._betas)

        # we can add this 1 in front to simplify indexing,
        # and to make alpha[0]=1 and sigma[0]=0.
        # these values at t=0 will be needed later when generating samples
        alphas_bar = np.concatenate(([1.], alphas_bar))

        return alphas_bar

    def alpha(self, t: torch.Tensor) -> torch.Tensor:
        return self._alphas[t.long()]

    def sigma(self, t: torch.Tensor) -> torch.Tensor:
        return self._sigmas[t.long()]

    def sample(self, x0: torch.Tensor, t: torch.Tensor, eps: torch.Tensor) -> torch.Tensor:
        """Draws samples from the forward diffusion process q(xt|x0)."""
        alpha_t = self.alpha(t).view(-1,1)#view(x0.shape[0] )
        sigma_t = self.sigma(t).view(-1,1)#.view(x0.shape[0] )
        return alpha_t * x0 + sigma_t * eps

class SinusoidalTimeEmbedding(nn.Module):
    """Time (positional) embedding as in Transformers."""

    def __init__(self, num_features: int, name: str = None):
        super(SinusoidalTimeEmbedding, self).__init__()
        self._num_features = num_features

    def forward(self, inputs: torch.Tensor) -> torch.Tensor:
        assert len(inputs.shape) == 1
        half_dim = self._num_features // 2
        e = torch.log(torch.tensor(10000.0)) / (half_dim - 1)
        embedding = torch.exp(-e * torch.arange(half_dim).float())
        embedding = inputs.view(-1, 1) * embedding
        embedding = torch.cat([torch.cos(embedding), torch.sin(embedding)], dim=-1)
        if self._num_features % 2 == 1:
            embedding = nn.functional.pad(embedding, (0, 1))
        return embedding

class UniformDiscreteTimeSampler:

    def __init__(self, tmin: int, tmax: int):
        self._tmin = tmin
        self._tmax = tmax

    def sample(self, shape: Sequence[int]) -> torch.Tensor:
        return torch.randint(low=self._tmin, high=self._tmax, size=shape)


class ResidualMLP(nn.Module):
    """MLP with residual connections."""

    def __init__(
        self,
        n_blocks: int,
        n_hidden: int,
        n_out: int,
        activation: str,
        name: str = None
    ):
        super(ResidualMLP, self).__init__()
        self._n_blocks = n_blocks
        self._n_hidden = n_hidden
        self._n_out = n_out
        self._activation = getattr(nn.functional, activation)
        
        self.label_emb=nn.Embedding(3,3)

        self.linear_input = nn.Linear(n_out+3, n_hidden)
        self.linear_time_1 = nn.Linear(n_hidden, n_hidden)
        self.linear_hidden_1_1 = nn.Linear(n_hidden, n_hidden)
        self.linear_hidden_1_2 = nn.Linear(n_hidden, n_hidden)
        self.linear_time_2 = nn.Linear(n_hidden, n_hidden)
        self.linear_hidden_2_1 = nn.Linear(n_hidden, n_hidden)
        self.linear_hidden_2_2 = nn.Linear(n_hidden, n_hidden)
        self.linear_output = nn.Linear(n_hidden, n_out)

    def forward(self, xt: torch.Tensor, time: torch.Tensor, label: torch.Tensor) -> torch.Tensor:
        
        c = self.label_emb(label)
        
        x = torch.cat([xt,c],1)
        
        x = self.linear_input(x)

        h=self._activation(x)
        h=self.linear_hidden_1_1(h)
        h=h+self.linear_time_1(time)
        h=self._activation(h)
        h=self.linear_hidden_1_2(h)
        x=x+h

        h=self._activation(x)
        h=self.linear_hidden_2_1(h)
        h=h+self.linear_time_2(time)
        h=self._activation(h)
        h=self.linear_hidden_2_2(h)
        x=x+h
        
        outputs = self.linear_output(x)

        return outputs
    
class DiffusionModel(nn.Module):
    """Diffusion model."""

    def __init__(self, diffusion_process, time_sampler, net_config, data_shape):
        super(DiffusionModel, self).__init__()

        self._process = diffusion_process
        self._time_sampler = time_sampler
        self._net_config = net_config
        self._data_shape = data_shape
        self.net_fwd = Net(net_config)

    #@functools.partial(torch.jit.script_method)
    def loss(self, x0: torch.Tensor, label: torch.Tensor) -> torch.Tensor:
        """Computes MSE between the true noise and predicted noise,
        i.e. the goal of the network is to correctly predict eps from a noisy observation
        xt = alpha(t) * x0 + sigma(t)**2 * eps"""

        t = self._time_sampler.sample( shape=(x0.shape[0],))  # sample time

        eps = torch.randn_like(x0, device=x0.device)  # sample noise

        xt = self._process.sample(x0, t, eps)  # corrupt the data

        net_outputs = self.net_fwd(xt, t, label)  # get net outputs

        loss = torch.mean((net_outputs - eps) ** 2)  # compute MSE loss between predicted and true noise

        return loss

    #@functools.partial(torch.jit.script_method)
    def loss_per_timesteps(self,  x0: torch.Tensor, eps: torch.Tensor, timesteps: torch.Tensor, label: torch.Tensor) -> torch.Tensor:
        """ Computes loss values at given timesteps."""
        losses = []
        for t in timesteps:
            t = int(t.item()) * torch.ones((x0.shape[0],), dtype=torch.int32, device=x0.device)
            xt = self._process.sample(x0, t, eps)
            net_outputs = self.net_fwd(xt, t, label)
            loss = torch.mean((net_outputs - eps) ** 2)
            losses.append(loss)
        return torch.stack(losses)

    #@functools.partial(torch.jit.script_method)
    def _reverse_process_step(
        self,
        xt: torch.Tensor,
        t: int,
        label: torch.Tensor
    ) -> Tuple[torch.Tensor, torch.Tensor]:
        """Computes parameters of a Gaussian p(x_{t-1}| x_t, x0_pred)."""
        t = t * torch.ones((xt.shape[0],), dtype=torch.int32, device=xt.device)

        eps_pred = self.net_fwd(xt, t, label)  # predict epsilon from x_t

        sqrt_a_t = self._process.alpha(t) / self._process.alpha(t - 1)
        inv_sqrt_a_t = bcast_right(1.0 / sqrt_a_t, xt.ndim)

        beta_t = 1.0 - sqrt_a_t ** 2
        beta_t = bcast_right(beta_t, xt.ndim)

        inv_sigma_t = bcast_right(1.0 / self._process.sigma(t), xt.ndim)

        mean = inv_sqrt_a_t * (xt - beta_t * inv_sigma_t * eps_pred)

        # DDPM instructs to use either the variance of the forward process
        # or the variance of q(x_{t-1}|x_t, x_0). Former is easier.
        std = torch.sqrt(beta_t)

        z = torch.randn_like(xt)

        return mean + std * z


    def sample(self, x0, sample_size, label):
        with torch.no_grad():
            x = torch.randn((sample_size,) + self._data_shape, device=x0.device)

            for t in range(self._process.tmax, 0, -1):
                x = self._reverse_process_step( x, t, label)

        return x

@dataclasses.dataclass
class NetConfig:
    resnet_n_blocks: int = 2
    resnet_n_hidden: int = 256
    resnet_n_out: int = 6
    activation: str = 'elu'
    time_embedding_dim: int = 256


class Net(nn.Module):
    """Combines MLP and time embeddings."""
    def __init__(self, net_config: NetConfig, name: str = None):
        super(Net, self).__init__()

        self._time_encoder = SinusoidalTimeEmbedding(net_config.time_embedding_dim)
        self._predictor = ResidualMLP(
            n_blocks=net_config.resnet_n_blocks,
            n_hidden=net_config.resnet_n_hidden,
            n_out=net_config.resnet_n_out,
            activation=net_config.activation
        )

    def forward(self, noisy_data: torch.Tensor, time: torch.Tensor, label: torch.Tensor) -> torch.Tensor:
        time_embedding = self._time_encoder(time)
        outputs = self._predictor(noisy_data, time_embedding, label)
        return outputs
    
# create the model
diffusion_process = DiscreteDDPMProcess(num_diffusion_timesteps=1000)
time_sampler = UniformDiscreteTimeSampler(diffusion_process.tmin, diffusion_process.tmax)
model = DiffusionModel(diffusion_process, time_sampler, net_config=NetConfig(), data_shape=(6,))

training_steps = 50_000

optimizer = optim.Adam(model.parameters(), lr=3e-4)

warmup_period=1000
num_steps = training_steps
t0 = num_steps // 1
lr_min = 3e-12
max_step = t0 * 1 + warmup_period

#model = nn.Linear(100, 10)

lr_scheduler = optim.lr_scheduler.CosineAnnealingWarmRestarts(
    optimizer, T_0=t0, T_mult=1, eta_min=lr_min)

warmup_scheduler = warmup.LinearWarmup(optimizer, warmup_period)

for step in range(training_steps):
    
    if step==30000:
        break
    
    lr = optimizer.param_groups[0]['lr']

    try:
        x0_batch = next(train_data_iter)[0].float()
    except StopIteration:
        train_data_iter=iter(train_loader)
        x0_batch = next(train_data_iter)[0].float()

    model.zero_grad()

    loss=model.loss(x0_batch[:,1:],x0_batch[:,0].long())
    loss.backward()

    optimizer.step()
    with warmup_scheduler.dampening():
        if warmup_scheduler.last_step + 1 >= warmup_period:
            lr_scheduler.step()
        if warmup_scheduler.last_step + 1 >= max_step:
            break
            
torch.save(model,f"/home/tau/aszatkow/work/crete_maiwen_titouan/DGE/naive_run_{idx_run}/gen_models/conditional_PCA_DDPM_6Comp_train-EUR_EAS_AFR_without_ACB_ASW-30Ksteps_REAL_TRAIN_80%_LAINET_PROCESS_2140hapseq.pth")