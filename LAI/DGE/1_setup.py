import os

lst=["01","02","03","04","05","06","07","08","09","10"]

for n_iter in lst:
    GENERAL_PATH=f"./naive_run_{n_iter}/"
    os.makedirs(GENERAL_PATH+"gen_models",exist_ok=True)
    os.makedirs(GENERAL_PATH+"temp",exist_ok=True)
    os.makedirs(GENERAL_PATH+"data",exist_ok=True)
    os.makedirs(GENERAL_PATH+"lainet_models_and_results",exist_ok=True)
    os.makedirs(GENERAL_PATH+"results",exist_ok=True)