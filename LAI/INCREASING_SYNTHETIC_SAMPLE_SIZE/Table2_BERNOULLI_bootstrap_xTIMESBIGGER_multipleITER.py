print('Start importing required libraries...')
import os, sys, time
import subprocess
sys.path.append('../')
from tqdm.auto import tqdm
import allel
import yaml
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import torch
torch.autograd.set_detect_anomaly(True)
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import math
from collections import Counter
import gzip
#import tagore
from scipy.interpolate import interp1d

import random

import argparse
import torch
import torch.nn as nn
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim as optim
import torch.utils.data
import torchvision.datasets as dset
import torchvision.transforms as transforms
import torchvision.utils as vutils

import dataclasses
from typing import Sequence
import functools
from typing import Tuple  # Add this line to import Tuple
from torch import optim
import pytorch_warmup as warmup

from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn import preprocessing

from sklearn.metrics import confusion_matrix

from lainet.models.network_constructor import get_network
from lainet.utils.eval import compute_accuracy, AccuracyLogger, complete_sk_eval, print_sk_eval
from lainet.utils.reader import load_founders_from_vcf_and_map, load_results_file
from lainet.utils.output_writer import get_meta_data, write_msp_tsv
from lainet.training import train_main, eval_predictions
from lainet.inference import inference_main 

from utils.format_file import add_result, naming_file, output_name
from utils.format_file import naming_file, extract_from_same_file, extract_from_file,output_name_mixed, extract_from_file_antoine

print('Done importing')

# Decide which device we want to run on
ngpu=1
device = torch.device("cuda:0" if (torch.cuda.is_available() and ngpu > 0) else "cpu")

if device.type == 'cuda':
    print(torch.cuda.get_device_name(0))
    print('Memory Usage:')
    print('Allocated:', round(torch.cuda.memory_allocated(0)/1024**3,1), 'GB')
    print('Cached:   ', round(torch.cuda.memory_reserved(0)/1024**3,1), 'GB')
 
# util function
def flatten(xss):
    return [x for xs in xss for x in xs]

def readVCF(filename,n_snps=500):
    """
    Parse output vcf of ancestry predictions along windows of sizes n_snps into pandas dataframe
    it takes the max ocurring ancestry in a window as the ancestry of that window
    """
    f = open(filename, "r")
    columns=["chm", "spos", "epos", "sgpos", "egpos", "n snps"]
    df = pd.DataFrame(columns=["chm", "spos", "epos", "sgpos", "egpos", "n snps"])
    currSNP=0
    spos=-1
    ancestryCount = []
    stupidL=[]
    epos=-1
    for line in f:

        if line[0] == "#" and line[1] == "#":
            pass
        elif line[0] == "#" and line[1] == "C":
            curr = line.split()
            for elt in curr[9:]:
                columns.append(f'{elt}.0')
                columns.append(f'{elt}.1')
                ancestryCount.append({})
                ancestryCount.append({})
                stupidL.append(f'{elt}.0')
                stupidL.append(f'{elt}.1')
            df = pd.DataFrame(columns=columns)
        else:
            curr = line.split()
            if currSNP==0:
                spos=curr[1]
            if currSNP%n_snps==0 and currSNP>0:
                newrow={"chm":curr[0],"spos":spos,"epos":epos,"sgpos":1, "egpos":1,"n snps":n_snps}
                for j in range(9,len(curr)):
                    for i in range(2):
                        max=0
                        ind=""
                        for key in ancestryCount[(j-9)*2+i]:
                            if ancestryCount[(j-9)*2+i][key]>max:
                                max=ancestryCount[(j-9)*2+i][key]
                                ind=key
                        ancestryCount[(j-9)*2+i]={}
                        newrow[stupidL[(j-9)*2+i]]=key
                spos=curr[1]
                df.loc[len(df)]=newrow
                #df.append(newrow, ignore_index=True)
            for j in range(9,len(curr)):
                anc=curr[j].split(":")[1].split(",")
                for i in range(2):

                    if anc[i] in ancestryCount[(j-9)*2+i]:
                        ancestryCount[(j-9)*2+i][anc[i]]+=1
                    else:
                        ancestryCount[(j-9)*2+i][anc[i]]=1
            currSNP+=1
            epos=curr[1]
            
    df.loc[len(df)-1,"epos"]=curr[1]
    
    return df

# Arguments
genetic_map_file =  "../data/input/real/allchrs_b37.gmap"
config_path = 'configs/default.yaml'

df=np.load("/home/tau/aszatkow/work/data/65k_all_labels.npy",allow_pickle=True)
#corresponds to 1000G_chr1_first_65535.recode.hapt file with intra-continental population label as first column
#super-population labels as second column and sample id (eg "HG00096") as third column, followed by 65535 remaining columns
df=pd.DataFrame(df)
df=df.loc[(df[1].isin(["EUR","AFR","EAS"]))].values #keep only EUR, AFR, EAS
idx_to_keep=np.unique(df[:,0])[2:] 
print(df.shape)
df=df[np.where(np.isin(df[:, 0], idx_to_keep))[0]] #Remove ACB and ASW
print(df.shape)

pair_idx=[(i,i+1) for i in range(0,df.shape[0],2)]  #when shuffling conserve the haplotypes pair representing one individual
np.random.seed(42)
np.random.shuffle(pair_idx)

shuffled_idx=flatten(pair_idx)

df=df[shuffled_idx]

df_train=df[:2416] #2416 corresponds to 80% of 3020

#at this stage df_train has EUR(820, 65538) AFR(792, 65538) EAS(804, 65538)

df_train=df_train[236:]

#then it has 2180 samples and EUR(750, 65538) AFR(720, 65538) EAS(710, 65538)

#the dataset with 2376 is when you apply Maiwen script so that it downsamples df[:2416] to 792hap seq per pop

#del df
print(0)

LEGEND_PATH="/home/tau/aszatkow/work/data/1000G_chr1_first_65535.recode.legend"
real_train=False
nSNP='65k'
pop=['AFR','EUR','EAS']
npop=len(pop)

encode_label_dic={"AFR":0,"EAS":1,"EUR":2}

GENERAL_PATH="/home/tau/aszatkow/work/crete_maiwen_titouan/ter-bioinfo/data/input/artificial/65k_SNP/"

for mult in [2,4,6]:#[1,5,10,25,50,75]: #frac of train to keep (+5)
    
    N=2376*mult
    N_pop=N//3
    N_pop, N_pop//2
    #where accuracy will be saved
    f=open(f"/home/tau/aszatkow/work/crete_maiwen_titouan/ter-bioinfo/Table2_BERNOULLI_bootstrap_{mult}TIMESBIGGER_multipleITER.txt","a")
    
    if mult==1:general_name_smap="SYN_TRAIN_80%_2376_EUR_AFR_EAS_without_ACB_ASW"
    else:general_name_smap=f"SYN_TRAIN_{mult}TIMESBIGGER_EUR_AFR_EAS_without_ACB_ASW"
    
    general_name=f"Table2_BERNOULLI_bootstrap_{mult}TIMESBIGGER_multipleITER-EUR_EAS_AFR_without_ACB_ASW_REAL_TRAIN_80%_LAINET_PROCESS_2140hapseq"
        
    SAMPLE_PATH="/home/tau/aszatkow/work/crete_maiwen_titouan/ter-bioinfo/generated/" + general_name_smap + ".sample"
    HAP_PATH = GENERAL_PATH+general_name+".hap"
    #convert to vcf
    OUTPUT_PATH=GENERAL_PATH+"reference_"+general_name+".vcf"
        
    for i in range(1,6):
        EUR=np.random.binomial(1,np.mean(df_train[np.where(df_train[:,1]=="EUR")[0]][:,3:],axis=0).astype(float),(N_pop,65535))
        AFR=np.random.binomial(1,np.mean(df_train[np.where(df_train[:,1]=="AFR")[0]][:,3:],axis=0).astype(float),(N_pop,65535))
        EAS=np.random.binomial(1,np.mean(df_train[np.where(df_train[:,1]=="EAS")[0]][:,3:],axis=0).astype(float),(N_pop,65535))

        df=np.vstack((EUR,AFR)) #used to be named "df"
        df=np.vstack((df,EAS))
        df=df.astype(int)

        #save to hap
        df = df.transpose() # .hap is, simply put, the transpose of our .hapt data
        df = pd.DataFrame(df)
        df.to_csv(HAP_PATH, header=False, index=False, sep=" ")
        del df
        
        bashCommand = f"bcftools convert --haplegendsample2vcf {HAP_PATH},{LEGEND_PATH},{SAMPLE_PATH} -o {OUTPUT_PATH}"
        subprocess.run(bashCommand, shell=True, env={'PATH': '/home/tau/aszatkow/miniforge3/envs/LAI/bin:' + os.environ['PATH']})
        os.system("gzip {}".format(OUTPUT_PATH))
        os.system("rm {}".format(HAP_PATH))

        reference_file_s=f"{OUTPUT_PATH}.gz"
        reference_map_file_s= GENERAL_PATH + general_name_smap + ".smap"

        output_basename=f"../data/output_lainet/65k_SNP/SYN_{mult}TIMESBIGGER_EUR_AFR_EAS_without_ACB_ASW_BERNOULLI/nITER{i}/"
        os.makedirs(output_basename,exist_ok=True)

        train_file=GENERAL_PATH+general_name+f"{N_pop//2}.vcf.gz"
        train_list_file=GENERAL_PATH+general_name+f"{N_pop//2}.tsv"
        train_map_file=GENERAL_PATH+general_name+f"{N_pop//2}.smap"
        
        data_folder_s=GENERAL_PATH

        nsamplepop=(N_pop//2)

        extract_from_file_antoine(reference_file_s,reference_map_file_s,nsamplepop,train_map_file,train_list_file,train_file,pop)#will create train_file, train_list_file and train_map_file
        #this function is to be find here: https://github.com/MaiDeme/LAI-Net/blob/main/utils/format_file.py

        os.system("rm {}".format(reference_file_s))

        chm              = 1
        #output_basename  = output_name('lainet',btrain,nsamplepop*5,btest,nquerypop*5)
        if (os.path.isdir(output_basename)==False):
            print('Creating directory...')
            os.makedirs(output_basename,exist_ok=True)
            print('Done')

        # Load vcf and map files and config
        config = yaml.load(open(config_path), Loader=yaml.FullLoader)

        print(config)

        #%time
        ## Training
        train_main(config, train_file, train_map_file, output_basename,chm,genetic_map_file)

        print("@"*20)
        f.write("@"*20+"\n")
        print("MODEL EVALUATION")
        f.write("MODEL EVALUATION"+"\n")
        for n_gen in [30,60]:
            test_file=f"../data/input/real/65k_SNP/REAL_VAL_20%_EUR_AFR_EAS_without_ACB_ASW_{n_gen}_gen.vcf"
            #%time
            ## Inference
            net, predicted, probs, val_snps = inference_main(config, test_file, output_basename, output_basename+f"query_test_{n_gen}_gen")

            print('Plotting results!')
            output_file = output_basename+f"query_test_{n_gen}_gen."+"msp.tsv"

            msp_df = pd.read_csv(output_file, sep="\t",header=1)
            msp_df

            df_test=readVCF(test_file)
            df_test=df_test.replace(encode_label_dic)

            conf_matrix = confusion_matrix(df_test.values[:,6:].astype(int).flatten('F'), msp_df.values[:,6:].flatten('F'))

            print("$"*3,f"BOOTSTRAP nITER{i}, Number of generations : {n_gen}")
            f.write("$"*3+f"BOOTSTRAP nITER{i}, Number of generations : {n_gen}"+"\n")
            for j in range(3):
                print(list(encode_label_dic)[j],":",conf_matrix[j,j]/conf_matrix[:,j].sum()*100)
                f.write(list(encode_label_dic)[j]+":"+f"{conf_matrix[j,j]/conf_matrix[:,j].sum()*100}"+"\n")
    #f.close()
    
        os.system("rm {}".format(train_file))
        os.system("rm {}".format(train_list_file))
        os.system("rm {}".format(train_map_file))
        #os.system("rm {}".format(reference_file_s))
        
    f.close()
