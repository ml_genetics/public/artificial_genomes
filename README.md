# Creating Artificial Genomes with Generative Models

- Artificial genomes (AGs) created via GAN, RBM and VAE models. Genomes from 1000 Genomes Phase 3 have been used for training.
- Genomes from 805_SNP files have 805 highly informative SNPs in terms of population structure.
- Genomes from 10K_SNP files have 10000 SNPs from chromosome 15.
- Genomes from 65K_SNP files have 65535 SNPs from chromosome 1 based on Omni 2.5 genotyping array framework.
- Rows are haplotypes and columns are SNP positions except for the first column which is group ID and the second column which is sample ID. Only biallelic SNPs are included with 0 and 1 tags for reference and alternative allele, respectively.
- Please refer to our [old preprint](https://www.biorxiv.org/content/10.1101/769091v2), [paper](https://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1009303) and [new preprint](https://www.biorxiv.org/content/10.1101/2023.03.07.530442v1) for further details and citation.
- This repository will soon be updated.
