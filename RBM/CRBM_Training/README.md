### Conditional Training using RBM

We considered a dataset of 10000 nodes. We train a classical (non-conditional) RBM on the first 5000 nodes of the dataset. Then, a CRBM is trained: the first 5000 nodes are used as the conditional configurations, while the model learns to generate the last 5000 nodes (given the first part)-

The files 
  * ExGENE_10K_L_0.py is used to train the non-conditional RBM
  * ExGENE_10K_L_CDT is used to train the CRBM

In the jupyter-notebook "EvalGene10K_CDT.ipynb", first the classical RBM is checked, and then the CRBM is tested.
