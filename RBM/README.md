#### Python code to train the RBMs and to evalutate them in both the Out-of-Equilibrium fashion and the Conditional setup.

Example of a scatter plot during the training of the RBM. In red the dataset, in green the generated data (from the RBMs). The scatter plots are projected along the first 4 directions of the PCA of the dataset. Right: the evolution of the eigenvalues of the weight matrix of the RBM during learning.
![Alt Text](anim_Gene805_.gif)
