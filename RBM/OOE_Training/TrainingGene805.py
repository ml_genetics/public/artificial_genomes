# Script Learning Gene805

import torch
import rbm


# CHOOSE CPU OR GPU
# device = torch.device("cpu") 
device = torch.device("cuda:0")
dtype = torch.float

# Load DATASET
import numpy as np
import matplotlib.pyplot as plt

path_dset = '../../TorchRBM.py/dataset/'
# randomize the dataset
X = torch.tensor(np.genfromtxt(path_dset+'1kg_xtrain.d')).float().to(device)

Nv = X.shape[0]
Ns = X.shape[1]
print('NumSamples=',Ns)
print('ShapeX : ',X.shape)


# Number of hidden nodes
Nh  = 500
# Learning rate
lr = 0.001
# L2 Regularization
l2 = 0.0
# Number of Gibbs steps to compute the negative term of the gradient.
NGibbs = 50
# minibatch size
nMB = 500
# Nb of parallel chains to compute the negative term of the gradient.
nNeg = 500

myRBM = rbm.RBM(num_visible=Nv,
				num_hidden=Nh,
				device=device,
				lr=lr,
				regL2=l2,
				gibbs_steps=NGibbs,
				UpdCentered=True,
				mb_s=nMB,
				num_pcd=nNeg)

# Initialize the visible biases
myRBM.SetVisBias(X)
# Use Out-of-Equilibrium learning
myRBM.ResetPermChainBatch = True
# STAMP for the file registering the weigths
stamp='TrainingGene805_OOELearning_Nh'+str(Nh)+'_lr'+str(lr)+'_l2'+str(l2)+'_Rdm_NGibbs'+str(NGibbs)
myRBM.file_stamp = stamp

# Number of epochs
ep_max = 20001
# Frequency of saving the weights
fq_msr = 20
myRBM.list_save_rbm = np.arange(1,ep_max+2,fq_msr)

myRBM.fit(X,ep_max=ep_max)
	

